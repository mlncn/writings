# Small Web Development Shops and Platforms

To my friends and colleagues, my fellow web developers.

If you're in the business of making web sites for people,

there are some laws of economics [does anyone actually call them that?] catching up with us.

1. Basic incentives
2. Economies of Scale

Look at all the companies founded by members of the Drupal community— Acquia, Pantheon, platform.sh—all offer some sort of proprietary lock-in.


Use simpler tools.  Scale, somehow.

This post is about the latter.

my personal motto, and there's always been some of that in Agaric.

the goal is liberty and justice for all, with a little old-fashioned shared prosperity and healthy respect—and respectful treatment—for the unimaginably complex ecosystems that make all life possible.

"The most power possible to people over their own lives" is an attempt make the sentiment real-world enough that the favored intellectuals of the ruling class

Those who do the hard work of [refining something until people want a new thing] deserve credit and compensation, and the platform cooperative world needs to work on that.


Flying into Newark, New Jersey you can really see how the concentration of people and economic activity around New York City makes grand things possible:  The many impressive bridges, the 

***

Drutopia notes


It's going to be atrocious to begin with, but we'll share.



***

An author's notes:  I really wanted to say 'prosaic' instead of 'real-world' and should probably just stick to 'concrete' in future essays.
