# On dealing with violent crime, taking as a starting point some facts about how things stand today

#PrisonAbolition
#PoliceAbolition

http://www.questionthepremise.org/new-blog/2015/12/2/dominant-narratives-white-media-and-jamar-clark

Links to #pointergate affair where basic police propaganda was that Mayor Hodges being in a photograph with a black man with a felony record was unconscionable, showing that they see every black man with a felony record as simply an intrinsic threat.

There are XXX,000 people with felony convictions in the United States (include people currently in prison/jail for that number)

A society that isn't planning its own sort of apartheid needs to come to terms with that.

But aren't there people who have done awful, unforgivable things?

Well yes, you could argue that— but the most revealing thing here is the lack of one-to-one overlap with the people we lock up and stigmatize versus the people who have committed heinous crimes.

As many as one in three women have been faced sexual violence, with between one and for and one in six being raped.  Around one in ten men have faced sexual violence.

Most of this sexual violence is committed by men.  Accurate numbers are very [hard to come by](https://fivethirtyeight.com/features/what-if-most-campus-rapes-arent-committed-by-serial-rapists/ ) but around one in ten men are rapists.  Nowhere near ten percent of men are locked up, were locked up, or are stigmatized.

Half [need to look this up to] of homicides go unsolved, by police departments' own metrics.

In short, there are XXX,000s of rapists and murderers running around free, with no record at all.

And we haven't even talked about business leaders and scammers (and the overlap there) and speculators and vulture capitalists who immiserate millions, spreading misery farther and wider than any common thief could ever hope to— not that a common thief probably would ever hope to spread misery.

The amount of serious crimes against individuals, against whole communities, and against humanity (think government war crimes) which are not punished at all utterly discredits the notion that we have to keep up a regime of extreme punishment against people who mostly have not committed such serious crimes, but are poor or marginalized.
