--
title: On the importance of being a cooperative (for real)
date: 2018-07-30
--

<aside class="searched-words">
	valve worker cooperative
</aside>



The [Guardian article attacking Valve's claims to no bosses and a flat hierarchy are false](https://www.theguardian.com/commentisfree/2018/jul/30/no-bosses-managers-flat-hierachy-workplace-tech-hollywood) is long-overdue pushback on headlines such as [Valve: How going boss-free empowered the games-maker](https://www.bbc.co.uk/news/technology-24205497).  Unfortunately, the pushback is from quite a reactionary direction.

The worker-cooperative movement has never claimed Valve corporation, but given that it was many people's first introduction to a company organizational model that claimed to get rif of bosses, we should have been much more vocal— especially since we could at least point to the much smaller but longer-lived [Motion Twin](https://store.steampowered.com/developer/MotionTwin): "an anarcho-syndical (seriously) workers cooperative that's been making games in France since 2001. No boss, equal pay, equal say. It’s an experiment and an experience!"  Read this [article](https://kotaku.com/game-studio-with-no-bosses-pays-everyone-the-same-1827872972) for more on Motion Twin; they plan to keep their flat structure by staying small.

We should have been citing Jo Freeman's [Tyranny of Structurelessness](https://www.jofreeman.com/joreen/tyranny.htm) right away and talked about the ways cooperatives seek to institute democratic structuring, and how [approaches continue to evolve to handle greater scale](https://gitlab.com/mlncn/presentations/blob/master/scaling-community-decision-making/worker-cooperatives.md) grounded firmly in principles of collective control and one person, one vote.

If you search for "Valve" and "worker cooperative" you're as likely to find a 1980 book, the [History of Work Cooperation in America](http://www.red-coral.net/WorkCoops.html), talking about emigration to the United States being Britain's attempt at a safety valve against revolution, or [new food-growing worker-owned cooperatives being formed in Western Massachusetts](https://www.masslive.com/business-news/index.ssf/2017/06/wellspring_harvest_cuts_ribbon_for_food-.html) on the site of the former Chapman Valve company, which presumably made, you know, actual valves.  (Or even the 51% government-owned "worker cooperative" [struggling in Venezuela](https://venezuelanalysis.com/analysis/2520) back in 2007.)

Within days of the leak, [two commenters](https://www.gamasutra.com/view/news/169063/From_the_editor_Valves_handbook_and_the_trust_phenomenon.php) talking about it from the perspective of [Distributism](https://en.wikipedia.org/wiki/Distributism) made the salient point that the workers in Valve's "flat hierarchy" are *just workers, not owners*:

> Casey Labine, 23 Apr 2012 at 6:38 pm PST:

> > if we can take this at face value

> Clearly it's stealth marketing, conveniently timed to build on Abrash's recent post[1], but still a (mostly) nice idea.

> It's not Distributism, though. The folks at Valve may benefit from better working conditions than the folks at EA, but they're still just workers. For example, the Narbacular Drop team doesn't own Portal; Valve does. Valve may treat the workers like they're owners, but at the end of the day they're still just workers. A game company that operates in the spirit of Image Comics would be much more interesting.


> Lars Doucet, 24 Apr 2012 at 6:12 am PST:

> @Casey: Indeed, the chief difference is that the workers aren't *really* worker-owners. That means shareholders are free to impose their will if they ever decide to. Right now that mostly means Gabe Newell.
>
> So, I wouldn't make the claim that the company is full-on distributist, but the values are certainly there in the employee handbook (which again, we can't necessarily take at face value)
>
> I'm cautiously optimistic :) We'll have to wait and see if they ever take the plunge to the Dark side.

Here's where someone representing the cooperative movement could have assured them that the plunge to the dark side is inevitable without a structure preventing it.

Even Yanis Varoufakis, who worked for Valve and so presumably literally drank the company's Flavor Aid or Kool-aid, was careful to make that distinction even while [celebrating Valve's allegedly greater freedom from hierarchy than most cooperatives](http://blogs.valvesoftware.com/economics/why-valve-or-what-do-we-need-corporations-for-and-how-does-valves-management-structure-fit-into-todays-corporate-world/).  A blog on the Peer to Peer Foundation site [led with that distinction when quoting directly from Varaufakis' blog](https://blog.p2pfoundation.net/valve-a-bossless-company-with-centralized-ownership/2012/08/05):

> There are two kinds of non-capitalist firms:
>
> (a) Mutual, co-op like, firms whose ownership is formally dispersed among members (who may be customers, employees or both); and
>
> (b) Valve (or similar companies) where management is completely horizontal (i.e. the company is boss-less) even if ownership is held in the hands of a selected few.

Here, too, the cooperative movement should not have let pass unchallenged this assertion that a firm can be non-capitalist while having the ownership of its capital concentrated.

That's not the way it's done, and the worker co-ops and the larger cooperative movement has good reasons for that.

Closest i've noticed is articles like this one:

> The rise in co-operative working in conventional businesses has received a cautious welcome from the co-operative movement.

https://www.thenews.coop/87829/sector/co-operation-but-not-as-we-know-it/


[Importance of replying publicly raised by Alex on the UK-focused CoTech Community forum](https://community.coops.tech/t/right-to-reply/943).

***

To read: Distributism in G.K. Chesterton's view, http://distributistreview.com/g-k-chestertons-distributism/
