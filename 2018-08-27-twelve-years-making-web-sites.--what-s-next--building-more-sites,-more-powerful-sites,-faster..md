---
title: Twelve years making web sites.  What's next?  Building more sites, more powerful sites, faster.
date: 2018-08-27
author: "Benjamin Melançon"
tags: ["Drupal Planet", "Drutopia"]
---


I've seen in the Internet (since before the web) a promise that is still unfulfilled—-a promise to connect all people who give a damn about something together so as to be able to do something about it. [My passion is connecting people so that they can take action, make their voices heard, and gain control over their lives.]

From education and a brief career in journalism I take an understanding in the centrality of distribution and some rudimentary writing skills.

I co-founded Agaric, a worker-owned software cooperative more than 12 years ago. Since then, I've learned a great deal about making web sites, working with a team, delivering for clients, and building a business.

I have helped found and have served on the boards of three not-for-profits (Amazing Things Arts Center, People Who Give a Damn, and Friends of Foster Care India) and helped a dozen others in various capacities. All organizations that I have worked with would have benefited from having easier to get started with, fuller featured web sites. [benefited from having access to tools that make it easier to connect to their audiences quickly and easily without having to sacrifice needed functionality that would facilitate the further growth of their organizations.]

I was co-founder and CTO of Activore, a social network and motivation app launched in independent New York City gyms. Activore enabled online social interaction among members and trainers at gyms and helped people bond and motivate each other as members of their fitness communities. Activore provided pilot gyms with a new way to reach their members that was more flexible than existing CRMs, but the vastly varied and API-less proprietary member management systems prevented a readily scalable solution. [[<<do not frame anything as a failure, what did you learn?]]

I am more than ready to take the focus and execution needed to take the Drutopia platform cooperative to the next stage of growth so that more people can get their message out and take action.

[I am a past recipient of a Knight Foundation grant and a Google Summer of Code Fellowship for my work connecting people to take action.]




Cut from https://pad.riseup.net/p/drutopia-coop-accelerator-TFVdhJVsBHFW




With few resources, they must dedicate them to not quite getting what they need or limit their own impact because of technology constraints.

Many organizations benefit from a website content management system, which allows sharing of posting and editing duties and many other features, but a CMS is



Claudina Sarahe bio: Past clients include PBS, Scholastic, Daniel Libeskind, Red Cross, and Casper where she built and scaled their front-end engineering team.]]







Throw in Clayton's proposal for Agaric:

## What

The landscape is changing for web services. More and more organizations are moving to SaaS solutions such as NationBuilder, SquareSpace and Wix. Others are turning to Wordpress. Those with larger budgets and more specialized needs still turn to Drupal to build their customized tools. However, the gap between those who can afford and not afford a Drupal site widens.

At Agaric we can do any of the following:

1. Focus our efforts on being more competitive in the larger budget space for custom Drupal builds
2. Support an initiative like Drutopia, which provides a similar SaaS experience, but through Drupal
3. Expand our skillset to other free software solutions.

I believe we could do any or all three. However, I believe pursuing the second best matches our expertise, market and values.

Currently our clients are nonprofits (MASS Design, Portside), health institutions (NICHQ), and education (TWiG, Patient HM).

## Why

So far we have relied on larger budget clients in these spaces to generate revenue. This is less than ideal for a few reasons: depending too much on any single client is risky, being a smaller team scattered across different codebases brings down our efficiency, a geographically dispersed team working on separate projects breeds isolation and providing custom solutions to those with smaller budgets is challenging.

If, however, we shift our business model to supporting a single (or series of related) platforms, we deepen our expertise with a single codebase, bring higher value to smaller budget clients and foster connections between one another since we are working on the same project. Finally, it means we can better generalize and share the solutions we build with the broader free software world.

## How

Our ideal situation would be to have several clients with mid-sized budgets, all on Drutopia, funding enhancements to the platform.

These are organizations that need a more customized solution, but benefit from being part of a more generalized platform like Drutopia. When possible funded development is contributed back to the platform, but there will be times that customizations happen only on their site.

The challenge in making that transition is that we do not have any of those ideal clients right now. So, as a transitional step, I propose we support a crowdfunding initiative that will build up a reserve of funds for us to use to fund our time improving Drutopia while also serving as a marketing campaign for us to find those ideal mid-sized clients.

Ideally we would raise $x to fund x months of development time for x people, including Nedjo and Rosemary of Chocolate Lily, who we have worked with in the past with great success.

I propose that Micky dedicate 15 hours a week (Micky, what's reasonable for you?) towards this crowdfunding campaign and I (Clayton) dedicate 10 hours a week. I can also dedicate an additional 8 hours a week outside of company time towards the effort. Anyone else who can contribute should to some extent, but between Micky and I we have many connections with free software enthusiasts and nonprofit organizers and technologists who are interested in contributing.
