---
title: Any software or other system adopted by an organization will change that organization's communication structure
date: 2018-09-09
status: draft
site: agaric.coop
---

Aurynn Shaw's Law

Inverse of [Conway's Law](http://www.melconway.com/Home/Conways_Law.html)
> Any organization that designs a system (defined broadly) will produce a design whose structure is a copy of the organization's communication structure.

Via [Aurynn Shaw on Enabling a Sustainable DevOps Culture](https://www.infoq.com/podcasts/aurynn-shaw-devops-culture):

> 5:50 Conway’s law as a way of framing our organisations. We make artefacts that encode our organisation,
> 6:03 The inverse of Conway’s law tells us that adopting tools and artefacts will change our organisation
> 6:15 Microservices architecture as an example of this in action – microservices is a team-organisational structure not a technical architecture
> 6:25 Microservices only works if you can structure your teams to the level where they can take ownership of individual microservices with clearly defined interfaces
> 6:57 Examples of how this plays out in teams and organisations.
> 7:38 How implementing Docker impacts on team and organisation structures
> 7:58 These are technical ideas, but they impose cultural constraints
