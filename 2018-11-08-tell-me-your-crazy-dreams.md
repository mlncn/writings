---
title: Tell me your crazy dreams
date: 2018-11-08
---

I did a lot of unsustainable pro bono or very subsidized work during Agaric's first few years.  For the past decade i've been pretty disciplined about explaining to people the cost of what they need, and not offering to work for free or at a steep discount.  Not surprisingly, since those early days, we haven't worked for many groups with wild dreams that are just getting started.

It's time to do work for people who can't afford custom development again.  [Tell me your crazy dreams.](https://visionsunite.org/)



***

This site will have autosearch while suggesting ideas, crowd-coalescing, and above all 





***

(my crazy dream)

will it do worse than what we currently have?  hard to believe


there's certainly the concern that literally random people making one-off


In the days before the 2018 mid-term election, the establishment media spent hundreds of hours of television and online video time and thousands of pages of newspaper and website space on a group of refugees, largely from Central America, entering Mexico with the intention of seeking asylum in the United States.  This had no current and vanishingly little potential impact on the lives of people in the United States, yet it had more coverage (ok i may be wrong here; i don't actually watch any media) than the spate of hate-motivated shootings in the same period.  And while mass murders explicitly linked to the white nationalism espoused by most elected officeholders and their propagandists in the private media is most certainly materially affecting people's lives, even that has hardly any impact compared to access to health care, clean water, clean air
