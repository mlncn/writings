---
title: Thoughts after watching The Young Karl Marx
date: 2018-11-25
publish: true
---

Always thought (from secondhand sources as much as the limited reading i've done myself) that Marx did a good job identifying flaws in economies based on the capitalist mode of production.  What he did not do was define in economic terms how a communist mode of production should operate.  His vision for society organized along different lines, with much leisure time for people to to develop themselves, practice philosophy and spend time in nature, is inspiring but seems relatively untethered from his core economic analysis.

Marx is also to be credited with making exploitation a, shall we say, materially graspable concept.


(while looking for a quotation i remember that spoke to time in nature, i found this presaging of modern "human resource" professionals talking about "human capital" and the need to work smarter—and take time off—not harder....)

"The saving of labour time [is] equal to an increase of free time, i.e. time for the full development of the individual, which in turn reacts back upon the productive power of labour as itself the greatest productive power. From the standpoint of the direct production process it can be regarded as the production of fixed capital, this fixed capital being man himself."


"Modern universal intercourse cannot be controlled by individuals, unless it is controlled by all"
Is that line, quoted in https://monthlyreview.org/2005/10/01/marxs-vision-of-sustainable-human-development/ , a call for ... democratically moderated mass communication?

The League of the Just (in Germany) proclaimed:

> The League aims at the emancipation of humanity by spreading the theory of the community of property and its speediest possible practical introduction.

Renamed the Communist League, under the influence of Marx and Engels:

> The aim of the League is the overthrow of the bourgeoisie, the rule of the proletariat, the abolition of the old bourgeois society which rests on the antagonism of classes, and the foundation of a new society without classes and without proper property.

http://socialistworker.org/2013/05/29/ready-for-the-revolutionary-wave

now i'm prepared to say i may have disagreed with them at the time, not just in retrospect.

This is what i was talking to Bridget about, before realizing just how clearly and forcefully Marx and Engels rejected what i was musing about.

The clarity of establishing sides— us versus them, proletariat versus bourgeoisie, is certainly valuable (even if you clarify, i hope, that we're eliminating social relations and therefore the classes, and are *not* eliminating the people who currently make up the bourgeois class).

But if we have a clear goal, the sides will become clear in who is helping us toward a goal and who is trying to prevent us from reaching it.

And this is where the usefulness of the focus on classes defined by their relation to the means of production comes into question.  Even if we accept that production of the material conditions for life is primary, and that everything else is secondary, there's an huge amount of really important stuff going on in that secondary areas: security/repression, culture/propaganda, coordinating/managing, public works, logistics...  child care would probably be counted by a Marxist as primary production but what about elder care?  Does putting all this second help with the cause of achieving justice, democracy, a classless society?

Exploitation comes from being forced to sell one's labor on the open market to survive.  If wealth were (relatively) equal, there would not be this kind of exploitation.  Abolishing individually-held productive property would not be necessary; indeed, equality of wealth would stimulate many new ways of holding and operating property in common (or rather, would greatly expand the scope and variety of cooperatives).

In one sense this is a meaningless point, because to get equality of wealth we do indeed need to wrest power from those that hold the wealth, whether called the bourgeoisie or bosses or owners.  And at that point, we can establish any social relations we want to govern our economy.  But that's just the thing.  What structures *do* we want to control our economy?  Iterating on what we have, changing it in ways we know will remove exploitation and unfairness, seems a lot safer than making something up wholesale— which actually inevitably means falling back on practices we have now which are likely to be exploitative and unfair.

I'm having a lot of trouble communicating this, sorry.

Obviously, the two opposing classes frame, those who sell their labor and those who own the means of production, takes us in a lot of interesting and fruitful directions.

But it's not the only nor necessarily the most productive frame.

How about:

Eliminating injustice, ending exploitation, increasing liberty, and gaining ecologically sound prosperity (would love a visualization of the proportion of current wealth tainted by its role in global warming) require equalizing power.

Power is organization.

Equalizing power can be achieved by organizing ourselves differently.

Redistributing monetary wealth (possibly achievable by changing the formula by which money is generated and distributed, else by global wealth taxation-plus-redistribution) is one such way.  But an analysis of present ways of organizing institutions in society, and how to organize so as to equalize power (while retaining what non-exploitative, non-repressive functions some institutions may serve) may be a more useful starting place for conceiving how to organize a new society and how to fight to get there.

But it's not like people haven't been thinking about this before, during, and since Marx's time.

... so maybe i can just jump to:

“Libertarian socialism, furthermore, does not limit its aims to democratic control by producers over production, but seeks to abolish all forms of domination and hierarchy in every aspect of social and personal life -- an unending struggle, since progress in achieving a more just society will lead to new insight and understanding of forms of oppression that may be concealed in traditional practice and consciousness.” — Noam Chomsky
https://twitter.com/dsa_lsc/status/1066874170460463105
