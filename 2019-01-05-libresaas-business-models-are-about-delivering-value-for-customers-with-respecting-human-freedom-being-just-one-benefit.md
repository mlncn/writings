---
title: LibreSaaS business models are about delivering value for customers with respecting human freedom being just one benefit
date: 2019-01-05
publish: true
---



> Start with the question “how can I deliver value for customers” and work backward from that. Then piece together the open source components you’ll need for your ultimate solutions that deliver value and construct your software supply chain.

https://medium.com/@johnmark/open-source-business-models-considered-harmful-2e697256b1e3

***

#LibreSaaS


## More from John Mark Walker:

### Why Your Open Source Project Is Not A Product

https://www.linux.com/blog/why-your-open-source-project-not-product

### How to Make Money from Open Source Platforms

1. https://www.linux.com/news/how-make-money-open-source-platforms
2. https://www.linux.com/news/how-make-money-open-source-platforms-part-2-open-core-vs-hybrid-business-models
3. https://www.linux.com/news/how-make-money-open-source-platforms-part-3-creating-product

### Open source and the software supply chain

https://opensource.com/article/16/12/open-source-software-supply-chain
