---
title: Scientifically-Sound Strategy
date: 2019-01-13
---

where there's enough data on primary metrics—those that affect your bottom line—we'll run experiments for all design decisions.

Where there's not, we'll bring our knowledge of sector-specific best practices to bear for you.  These are especially informed by our involvement with building platforms, which make it easier to gain applicable, scientifically valid, knowledge.

our approach is informed by:

  * Agile practices
  * Lean startup methods
  * Lean UX and Erika Hall's Just Enough User Research
  * Value-Based Design,

grounded in on-going research into the motivations and actions of the people who matter most to your organization— be that clients, customers, members, or   
