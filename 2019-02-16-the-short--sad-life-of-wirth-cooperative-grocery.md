---
title: The short, sad life of Wirth grocery
date: 2019-02-16
---

"Wirth Co-op won't reopen", a tiny heading on the front page of North News announced.  The [story](https://mynorthnews.org/new-blog/2019/1/17/wirth-co-op-will-not-reopen) ran inside on page four.

The grocery store, right in my neighborhood, set out to build community wealth and provide healthy food in north Minneapolis.  The loss of its promise is crushing.

Wirth Grocery's failure to act as a cooperative—with its own existence at stake—is even more upsetting.  Cooperatives are one of the few spaces where people have a chance to take collective control of something that matters to them.  The grocery store was open for a brief time, but it was never a cooperative.  I know, because I joined as a member the day it first opened in 2017, on August 11.  (I first e-mailed to ask about becoming a member ten months earlier, but that required meeting in-person and this was the first time it was feasible.)

On 2018, June 12, after Wirth Cooperative Grocery had been closed for two months, I received my only formal communication as a member owner: an e-mail acknowledging that the store was closed, noting that the current grocery market is extremely competitive, and saying they had plans to re-open by the end of the month.

The e-mail did not ask for decisions.  It did not ask for volunteers or help of any kind.  While addressed to us as member owners, it did not afford us any opportunity to affect the future of the store.  It did not provide any information on which we might try to act.  Instead it told us to wait to be notified when an opening date was set.  An opening date was never set.

Although I'm certain some staff and volunteers worked hard behind the scenes, from my perspective as a member owner the grocery store went down without a fight.

That's why it's so important for cooperatives to be true cooperatives.  The [seven cooperative principles](https://www.ica.coop/en/cooperatives/cooperative-identity) aren't a "pick any three" sort of deal.

The first principle specifies that membership in a cooperative be open to all people willing to accept the responsibilities, but a person cannot accept responsibilities which aren't offered.

The second principle is democratic member control, but people cannot exercise direct control or hold representatives accountable without information and without a means to communicate with one another.

Likewise for the next three cooperative principles: the third, that members democratically control capital; the fourth, that external agreements preserve democratic control; and the fifth, that a cooperative educate, train, and inform members so they can contribute to it effectively.  An organization with no mechanisms for discussion nor for democracy violates these principles, too.

Principles six and seven, cooperation among cooperatives and concern for community, are likely to be hollow without functioning internal democracy— and certainly cannot be realized if the business fails.

A cooperative can't exist only on good intentions.

When I hear this sentiment expressed by experienced cooperators—founders and developers of cooperatives—it usually means that there needs to be a solid business model for a cooperative, because a cooperative that isn't economically viable can't fulfill any of its goals.

A more fundamental meaning is that a business can't be a cooperative if it merely intends to be; it must act like a cooperative.  Otherwise, it's just another business with some co-op lip service gloss— and given the [greater success of cooperatives compared to other businesses](http://www.geo.coop/story/benefits-and-impacts-cooperatives) it's less likely to be around as a business at all, if it does not live up to its cooperative principles.

I'm not trying to use technicalities to dodge counting the failed Wirth grocery store as a failure for "team cooperative".  On the contrary, this is a wakeup call for everybody who supports cooperatives, one that must rouse us, because fully-functioning cooperatives are bigger than the cooperative movement.  Cooperatives can prefigure true democracy.  We need to show that economic democracy works in our voluntary organizations; we need to give people in a world which is already ruled unjustly, and threatening to spiral out of control, a promise and a practice for how whole communities can take collective control.

In my experience as a member owner, Wirth Cooperative Grocery was not a co-op beyond its name and some vague intentions.  Now I know that my experience matched everyone else's, thanks to Cirien Saadeh's reporting, both [last year](https://mynorthnews.org/new-blog/2018/10/29/wirth-co-op-is-still-closed) and in the most recent issue of [North News](https://mynorthnews.org) (an invaluable local [non-profit enterprise](https://www.puc-mn.org/)).

What worries me, then, is that no one quoted in these articles called out this failure to meet the basic requirements of being a cooperative.  Minneapolis and Minnesota have perhaps the highest rate of cooperative membership in the United States, with about half of all residents estimated to belong to at least one cooperative of one kind or another.  If we, here, don't have the awareness and interest to note when a cooperative doesn't act like a cooperative, who will?

More important than calling out failures is providing pathways to success.  There are many programs and organizations [supporting cooperatives in Minnesota](https://agaric.gitlab.io/raw-notes/notes/2019-02-18-resources-for-cooperative-enterprises-in-minneapolis-and-minnesota/) and beyond, but none put ensuring member control first.

The bias of my [profession](https://agaric.coop/services) and my [passion](http://mlncn.com/node/744) is to lead with an online tool: ensure member owners can communicate with one another.  Although [a technological fix can't solve political problems](https://sensusjournal.org/2019/02/12/why-a-technological-fix-cant-solve-political-problems-the-case-of-cape-towns-water-crisis/), people who are affected need a way to talk among themselves to *come up* with any solution.

Efforts like a local cooperative grocery are small enough that a [Loomio group](https://www.loomio.org/) or any [mailing list](http://www.list.org/) would mostly work for member owner discussion.  A better software application would work for collaborative groups of any size: members would filter messages for quality without ceding control to any outside group.  This self-moderation for groups of equals, including cooperatives, is a goal of [Visions Unite](https://visionsunite.org/).

Are you in a position to provide resources to cooperatives and other groups seeking to grow and be democratic?  Are you starting or do you want to start a cooperative or group?  Do you have other ideas on how to help new and established cooperatives live by the foundational cooperative principles?  I would like to [hear from you](https://agaric.coop/contact)!



***

***

.  Sure, the effort has been in crisis since it began, but if member-owners aren't called on in times of crisis.




maybe it's just the way i operate, but i expect friends and family i haven't talked to in years to contact me if they need me.



pleading to let members know what's happening and how we can help


$70,000 of community members' money, $500,000 of federal grant money,





just because sortition makes large-group democracy manageable doesn't mean it will be managed.  The messages people are asked to engage with are exactly the unmediated ones.  We're basically asking a few people at a time to take the figurative bullet for the larger group.




I am member number 2712.  (were there really more than 2,700 members?  I don't think so, the newspaper report cites 700, but i don't know this most basic piece of information members should be provided)


both a filter to make sure messages being sent to


Via mom:

you need to condense, fewer words.

"We need to give people in a world already ruled unjustly, threatening to spiral out of control."  Wherever you can remove non-meaningful words will allow the meaning to come out more clearly.  Name principles one at a time.  Extra words are confusing rather than clarifying.