---
title: open letter to people working on political campaigns
date: 2019-02-24
---

Dear everyone working on political campaigns to try save the world from burning:

You will be helping build powerful communication infrastructure.  It is critical that we not build this infrastructure to be under the control of particular candidates or even parties.

Our future depends in large part on our having control over our communication.  To have all the work of reaching millions of people thrown away or co-opted