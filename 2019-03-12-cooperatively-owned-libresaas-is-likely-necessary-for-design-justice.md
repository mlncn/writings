---
title: Cooperatively-owned LibreSaaS is likely necessary for Design Justice
date: 2019-03-12
---

Yes, you, with your hand raised in the front.

Can I define all the words in the headline?

Um... most of them; I may not be able to give you the definition of "is" [link to Monica Lewinsky's take on this, if she has one, and if not and probably even if so drop this joke of an introduction]

Agaric does a lot of migration work, and for a presentation Clayton led at the Drupal/WordPress/etc. pre-conference day at the Nonprofit Technology Conference we looked up the export capabilities of all the major website platforms.

Wix – Officially, ["cannot be exported", "not possible"](https://support.wix.com/en/article/exporting-or-embedding-your-wix-site-elsewhere)

That's unethical business practices right there.

Libre software would mean that people would have hacked on


The business structure of a platform cooperatively owned by the people who use it makes it more likely that data and content export functionalities will be supported.  The interests represented are those of everyone using the platform, including those with the interest in leaving it someday.  A non-cooperative platform is likely seeking only


Design justice is a set of principles and an evolving theory, practice, and ways of teaching about itself.

The principles themselves are also still open to further definition.

[screenshot of that article about that close-but-not-enough fundraiser's death
people dying because their GoFundMe was short $50

[screenshot of chart or other data visualization of the life expectancy gap]
the life and death outcomes of injustice are not usually that dramatic, in the literal sense of an emotion-affecting narrative, but the overall life expectancy gap of [look up, confirm] 15 years between the richest ten percent and the poorest ten percent sure is dramatic in the figurative sense of goddamn that's bad.

the lottery by Shirley Jackson

and even if if's not, or when it's not, a matter of life or death

"sorry you weren't born rich; your odds of fulfilling ambitious life dreams just dropped by a hundred orders of magnitude"


LibreSaaS: The business model for open source that restores the liberatory potential of the Free Software movement.



(If someone challenges me by something as stupid as asking how much money i personally make, i want to respond by saying: "The point was headed straight for your head, and you turned and smashed your face into a wall instead."  And then move on without further commentary.)