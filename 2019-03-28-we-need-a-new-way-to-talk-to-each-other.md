---
title: We need a new way to talk to each other
date: 2019-03-28
---

We need a new way to talk to each other.  Not one-to-one, I have no idea how to improve on interpersonal relations.  But in large groups.  Groups large enough to wield power.  

The goal of people who give a damn throughout history is greater freedom and justice for all, equally.  Power requires organization which requires communication.  Control over communication is a major lever of power.

We need to build more powerful networks, but we need to do so in a way that preserves equal power for all.

[mutually reinforcing trend-following outlets and algorithm-driven dissemination is flooding the culture with sameness](https://longreads.com/2019/03/29/on-flooding-drowning-the-culture-in-sameness/)