---
title: Commitment to Accessibility
date: 2019-03-31
---

This is an embarrassingly late date for a post with this title.

We've been including a clause about accessibility in all our proposals (which serve as simple contracts) for a few years already:

> ## Accessibility
>
> It is critical that movement websites be accessible to as many people as possible, including those using screen readers. To that end, all of our work is built to support compliance with [W3C Web Content Accessibility Guidelines (WCAG) 2.0](https://www.w3.org/TR/WCAG20/). The WCAG Guidelines, however, are not comprehensive and so we go beyond those guidelines to ensure high accessibility. Lastly, much of what makes a site accessible happens on the content entry and management side. We will provide resources for content editors so that after the site launches, new content reaches as many people as possible.

And we've had a commitment to accessibility from the start.  We just haven't written about it before.

is committed to diversity and to ensuring that our programs, services and activities are accessible to all. Part of that commitment will be seen in work to safeguard that our website conforms to accessibility standards, thereby satisfying the existing or potential requirements to make this website comply with the ADA (Title II) and Section 504 of the Reauthorized Rehabilitation Act.

seeks Information and Communication Technology (ICT) that is universal in design and accessible to all individuals, including individuals with disabilities. In the event this is technically infeasible or imposes undue burden, the University ensures an equally effective accessible alternative. All ICT must meet the applicable accessibility standards set forth in Section 508 of the Rehabilitation Act of 1973, as amended (Sec. 508), the Americans with Disabilities Act of 1990, as amended (ADA),

[todo keep reviewing https://www.unr.edu/accessibility/policy]


(ripping off the highest SEO for "commitment to accessibility" on Duck Duck Go, which happens to be the University of Nevada, Reno)