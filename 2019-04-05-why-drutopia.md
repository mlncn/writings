---
title: Why Drutopia
date: 2019-04-05
---

Alternate title: What must be done (from the perspective of a person in Drupal)



Two threads:

* Multiple crisis hitting humanity and the planet
* Downward slide of Drupal and other massive collaborative projects

which i will attempt to weave together in a way that it doesn't end in disaster, at least not too long before the heat death of the universe.


In political science, there's the classic problem of concentrated benefit and distributed cost.

The solution is communication and coordination tools and networks capable of giving the distributed many the same degree of discuss, decide, and direct organizational power of the concentrated few.

That extreme wealth correlates with interests that run against humanity's as a whole adds an extra challenge.

The fact that a few people with much to gain (or lose) will typically have a much greater impact on what happens than a far larger number of people with individually less, but collectively more, to lose (or gain) has some unfortunate corallaries.

For instance, James D'Angelo has done convincing research showing that the move to public votes in the US House of Representatives in 1970 increased the power of big donors, lobbyists, and political parties.  Before, when most votes were secret, representatives had the option of lying to their donors, to lobbyists, to party "whips", and thus to vote their conscience.

When transparency possibly means more corruption,


Give people a way in to using and developing libre software.

Give people and organizations software they ultimately have control over.

... while also giving people a way to use the software without either great expense or great expertise.

Develop a revenue source for basic software development (as opposed to custom development), and use it to bend the world toward justice, in terms of who is paid to build it, and what it is built to do.



***

i like to say justice and liberty, it's prettier, but harder to make sure you're talking about the same thing.  Famous douchebags like Hayek made a whole career of defining liberty as .  As much fun as it is to destroy an argument that hinges on claiming a person in a pit is as free as a person not in a pit, ain't nobody got time for that.

So that's where i'm coming from.

You might ask— if you're so set on changing the world, why do you have a small business that makes web sites?  And to that, i would say: Fair question.

I had a lot of disposable income, somehow, working the overnight shift at an extreme discount retailer called The Christmas Tree Shops, before co-founding Agaric, and had already helped support some people doing really good things in the world but having to beg for money in order to do it.  I didn’t want to be in that situation.  After more than a decade in business, we can say that's working.  The going out and doing good in the world, not so much.


***

In Drupal, the benefits of a LibreSaaS offering are manifold

the threefold aspects

community
 - new adopters can get started, knowing they don't have the dead-end or Wile E. Coyote situation of the ground being pulled out from under them

code
 - economic base for code contributions

customers
 - convenient
 - no lock-in

this can apply to many software communities

Drupal 8 is particularly well-suited for LibreSaaS

***


So what do we need to do about this?  In every case, we need to gain power to make change.

“Knowledge is not power,” said Mariame Kaba (prisonculture), who is highly knowledgeable on the subject as a longtime organizer focusing on ending violence, dismantling the prison industrial complex, practicing transformative justice, and supporting youth leadership development.

Which is why i’m triply excited about LibreSaaS as a software and platform cooperatives: it is tech that gives us the power to coordinate; building it is an exercise in coordination; and we need to build more tech to coordinate to do it all.