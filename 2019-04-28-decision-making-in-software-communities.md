# Decision-making in Software Communities









## Promise of digital and Internet 

https://twitter.com/carljackmiller/status/1122459930504650752

"values and standards and ethics and in fact truth could depend on what your view of the world is, and there might be as many views on that as their are people."  "uniting them instead in an electronic community"  "in a future world that we would describe as balanced anarchy, a   no single privileged way of doing things, able to   waste of human talent that we couldn't or wouldn't do.  if the universe is whatever you say it is, then say"



## People are already recognizing the scale problems with tools like Slack

Sam Kottler Retweeted
Jorge Ortiz @JorgeO
May 2

Jorge Ortiz Retweeted Jorge Ortiz

New proof-of-work idea for Slack usage of @-here and @-channel in large channels:

Slack randomly samples ~7 faces of people who will get dinged, asks you: a) what is this person's name? b) is your message relevant to them?

Jorge Ortiz added,
Jorge Ortiz
 @JorgeO
Replying to @Lethain
Counterpoint: Slack warns you how many people will get notified with an @-here or @-channel, and people *still* misuse them.
4 replies 3 retweets 21 likes


https://twitter.com/JorgeO/status/1124018161001562112
