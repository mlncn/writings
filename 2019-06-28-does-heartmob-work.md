---
title: Does HeartMob work?
date: 2019-06-28
---

HeartMob seems like a great idea, or more than that, at first look or description it feels like a good implementation of a necessary mechanism.

But does anyone find that it works?

Let me modify that.  Does anyone find that it succeeds in any of its goals *beyond messages of support*?

when there's no action i can or will take to in fact *have your back*, it feels pretty empty to say it, and after a while, i imagine it feels pretty empty to hear it.



Michelle Curran


Keynote:
Black guy, maybe from Africa, but very little accent, sounded like he said Kevin Aguilar?  Political.  What culture do you want to promote?




> It is better to remain silent at the risk of being thought a fool, than to talk and remove all doubt of it.
— Maurice Switzer

per https://quoteinvestigator.com/2010/05/17/remain-silent/