---
title: Towards shareable configuration in Drupal 8, 9, and beyond
date: 2019-07-23
---

Twelve years ago, [someone on Drupal.org suggested a default role for administrative duties](https://www.drupal.org/project/drupal/issues/182023):

> The rationale is simple: core and contributed modules could set intelligent defaults for an administrator role, just like many do for anonymous and authenticated roles.

Another approach could be variable substitution to swap the defined roles or content types in a feature for ones existing on a site.  There's many technical problems with this, though, and we can start right now, without developing any further tools, to divvy up our configuration into the smallest shareable pieces right now.