# A resident dividend to mitigate gentrification?

Riffing off of [Grace Holley's tweets about tourism-driven gentrification](https://twitter.com/hollley/status/1162075515534094340), i wrote:

As part of the general gentrification solution, what do you think about a resident dividend?  Every property owner pays ~10% tax on value of property, every resident gets an equal monthly dividend worth an equal share of 80% of the total that's collected.

Homeless, renters, small-and-average-value property owners (up to almost the mean, a majority of people in unequal places) get money instead of paying; wealthy estate owners and absentee landlords pay more than now; local governments still get their ~2% property taxes.

(Albeit at likely lower overall lower tax valuations; know anyone who could do economic modeling on this?  But the same government would be serving fewer high-need residents as receiving $10K+ a year sure would help people with the least resources a lot.)



Here's some discussion about what $500 a month more means for families in poverty: https://www.thenation.com/article/stockton-california-michael-tubbs-poverty-basic-income/

A resident dividend at the level discussed above would be more like $1,000 a month *per person* in poverty, so several thousand dollars a month for a family.

