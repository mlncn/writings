---
title: If you want to hate on Christmas do not profess to speak for an entire religion
date: 2019-12-19
---

This is a quick response to "What does Judaism say about Christmas: A night we don't study, a day soaked in sorrows, and today's materialism", which was [posted]( ) on Sefaria Source Sheet by Yonah Bookstein.

Sefaria is a repository for research directly citing primary sources; it's not a magazine or anything with editorial approval.  Bookstein is a rabbi; him putting notes on Sefaria Source Sheet rather than his synagogue's or his own website seems to be the rough equivalent to me, as a web developer, posting technical musings to [the Agaric raw notes on GitLab](https://agaric.gitlab.io/raw-notes/ ) as opposed to [the official Agaric blog](https://agaric.gitlab.io/raw-notes/ ).  Also worth noting that, as a rabbi, the title which i object to is potentially 

Nevertheless, it is on the Internet, and is making the rounds enough that it happened to get put in front of a group i'm in, as a very interesting read.  (Update: Turns out the vast majority of Bookstein's article is copy-pasted from an article by Lawrence Kelemen, remixed to put the imagined "Hitlerday" history first.)

The cited materials are often informative, and part of me wants to embrace every last wild claim as ammunition to finally start fighting the [imaginary war on Christmas](https://www.mediamatters.org/war-christmas/war-christmas-story-how-fox-news-built-dumbest-part-americas-culture-war ).  But the very framing is reactionary.

The essay quotes 1600s Puritan minister (and the first president of Harvard), Increase Mather, as writing in 1687 that "the early Christians who first observed the Nativity on December 25 did not do so thinking that Christ was born in that Month, but because the Heathens' Saturnalia was at that time kept in Rome, and they were willing to have those Pagan Holidays metamorphosed into Christian ones."

The facts are largely correct, but the appeal to this particular authority is problematic— and revealing.

Increase Mather was the spiritual advisor to the slaughter of Native Americans in New England during the Native's people fight for survival led by Metacom.

Increase Mather was also an unrepentent supporter of the Salem witch trials.

Bookstein, in his broadside against Christmas which tries to link it directly to genocide and 

The [modern meaning of Christmas for Jews is just a day when almost everyone else is doing something else](http://www.jewfaq.org/xmas.htm )— and of the various things Jews are doing on December 25, observing [the entertainingly combative but little-known Christmas Eve counter-holiday](https://en.wikipedia.org/wiki/Nittel_Nacht ) doesn't even rank in Jewish popular consciousness.  (on JewFAQ.org which is a site run by a Jewish person, Tracey Rich; there are unfortunately plenty of websites with names like JewWatch run by virulent antisemites)


(A minor point, but the one piece of evidence used to tie Christmast to commercialism, which, let's be honest, doesn't need any supporting evidence which we aren't being inundated with constantly in late capitalist United States, the [story that Coca Cola made Santa's coat red, happens to be false](https://www.snopes.com/fact-check/the-claus-that-refreshes/ ).)

what's  https://newrepublic.com/article/155955/trump-creepy-white-house-christmas-aesthetic-unified-theory


This matters not because Christmas is great and perfect and completely unproblematic.  This matters because it's much harder to completely reject a part of the dominant culture than it is to transform it.  The story of Christmas, a pagan holiday embraced and partially changed my Christians, teaches us that if nothing else.

Does that mean we shouldn't seek to reject anything?  No, of course not.  If human sacrifice were .  That is why people are working to end poverty, police, prisons, prejudice, pollution, cars, global warming, war— some of the dominant ways we sacrifice humans today.  (Note that heart disease, listed as the top killer in the United States, [takes a much greater toll because of poverty](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2803292/ ).)

is a message of 

> He said: Don't be judgmental. Don't be a hypocrite. Don't use your religion to bully others. Don't pray loudly in the streets to show how holy you are. Forgive others. Heal the sick. Feed the poor. You're not without sin, don't cast stones. By your deeds you will earn your place in heaven. Be humble in spirit, be generous in nature. LOVE ONE ANOTHER.

https://www.patreon.com/posts/31438903

This broadside against Christmas tries to link it directly to genocide— a very bad thing, but the connections of the holiday in particular are tenuous to the extremely well-documented tendancy of large groups of Christians trying to kill us.

The article also links Christmas to pagan traditions— and that is very well supported by historical evidence, the problem here is that it is presented as a bad thing.

is subverting social norms and making visible the possibility of 

trying to taint a two or three thousand year old celebration with every negative connotation that has come near it can be an entertaining read, and I would probably read it in the spirit of challenging established dogma if it were written by a Christian or at least not given the title "What does Judaism say about Christmas".

To the extent that Christmas centers pagan traditions, it's an opportunity for people to reclaim joyous practices that subvert social norms and making visible the possibility of equality.

To the extent that Christmas centers Christ, [routinely the most progressive voice in the New Testament](https://www.patreon.com/posts/they-told-me-to-31438903 ), the messages can be used to [](https://www.straight.com/news/513956/what-our-leaders-arent-telling-you-jesus-was-radical-social-activist )

and i am all for [ruining your friends and relatives holidays](https://thedailybanter.com/2016/11/thanksgiving-donald-trump/) if trying to get them to stop supporting locking children in cages ruins it for them— but guilt by association for a syncretic holiday celebrated by a world religion whose adherants have committed at least their share of crimes against humanity— well, the holiday isn't the major problem there, and there are plenty of specific things to criticize and try to end or change within the religion that would be more effective and accurate than an encompassing attack.
