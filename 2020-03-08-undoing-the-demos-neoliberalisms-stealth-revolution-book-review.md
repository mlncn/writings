---
title: Undoing the Demos neoliberalisms stealth revolution book review
date: 2020-03-08
---

***

Finished the book, and all its footnotes, today.

As anyone who has read two sentences (sometimes just one) that I've written knows, I'm not opposed to tangents.  (That's why i was so excited when reading Victor Hugo, and realizing this is considered great literature and he has three page asides about economic and social justice.  That is my jam.)

So I'm cool with Wendy Brown closing out the book with a consideration of human sacrifice.  But I do think that if she could get into that, a book about the hollowing out and ultimate subversion of democracy, acknowledged in the book as allowed in part by the reducing democracy to occasional formal votes, could have explored the possible countervailing force from workplace democracy.

