# Going online together to save our lives

Anthropologist Kasey Qynn Dolin noted of our societal moment that <abbr title="COronaVIrus Disease which emerged in 2019">COVID-19</abbr> has thrown into sharp relief:

> We expected going online would lead to mass democratization and access to information that empowers large groups of people.
>
> Instead, we got outsourced.

Online connectivity gave people who own means of production the ability to outsource work in a way that offloads expenses— the cost of the things which generate the conditions for the wealthy and powerful to profit in the first place.  Some of this, like shifting the expenses of maintaining fleets of vehicles off of a corporation's balance sheet, have been celebrated as technology-enabled innovations in business models.  This is usually a big charade, a technological gloss on what is just another, relatively minor, attempt to concentrate wealth and power.  Meanwhile, the biggest return on capital and the biggest harmful effects on people come from practices that business media don't focus on.

The world going online has helped the owning class shift the costs of workers simply living (health care, child care, retirement, sustenance or more-than-sustenance wages) onto people, communities, and whole nations less able to hold global corporations to account.

There are a couple big reasons this happened.

First, technology is not neutral, and it tends to serve the needs of those who shape it.

Even with the incredible historical happenstance of US military money flowing to academic researchers who developed and incubated an Internet that was computer-to-computer rather than hierarchical…

…and even with the impressive tradition of technologists making decisions openly and favoring adopting standards by rough consensus that allow unaffiliated parts to work with one another…

…the services built on the Web and other Internet-based protocols and standards reflect the desire for control (for wealth and power) held by corporations, governments, and venture capitalists— because these are the entities that have been spending the money and increasingly calling the shots for the past twenty years.

Second, the same reason a miracle was required to make the Internet distributed in the first place has worked against many of the technology built on the Internet being peer-to-peer— it's hard to come to agreement on protocols and hard to make them easy to use.  It's easier to build software and services ultimately within one organization's control, and the organizations with the resources to do even this have tended to be the same ones centralizing power (with notable exceptions such as Wikipedia and the Internet Archive).

Similarly to networked computers, in the world of organized humans it's very hard to develop ways to build shared power.  It is much easier to use communications technology to move work and consequences farther from those who already hold centralized power.

However, these facts working against us don't mean we have to take our struggle for our human rights and justice entirely offline.  And struggle we must, because the people who hold wealth and power now are seeing, in this health and economic crisis, just how much can be denied us without threatening their wealth and power.

A lot of the technology developed for business as usual, even the proprietary stuff we don't actually control like Slack and Google Docs, can be put to use by movements for justice and liberty.  There are a lot of similarities in what people need to work together, whether it's in companies or in community groups.  That's not to say that the movement to build truly free as in freedom alternatives to these is not good and important. Indeed, the development of free libre open source software is downright great and vital when led by or done in direct collaboration with groups marginalized by capitalism that do have different needs, such as a threat model that must take into account government repression and fascist threats.

But we need to concentrate our finite time on building technology corporate and political masters would never want built.

We need to focus on that which builds shared power.  **We need to build ways to share power.**

Power is organization.  People organized in large groups can hold even global corporations to account.  We can replace existing structures, as proves needed, to gain the sustenance needed for life; to gain the justice of a common share of all that nature has given and all that humanity has worked for; and to gain the freedom to do what we will with it all.

Any group with that level of power is unlikely to keep true to universal human rights and and all the rest unless power stays distributed within it— ideally throughout all society.

Formal democracy, however flawed or perfected, has never been enough and will never be enough.

We need ways of communicating and making decisions that work well and keep power distributed.

Sharing the power inherent in controlling communication and in making decisions requires sharing the work of both.

There's endless creativity required in getting people to see a better world is possible, and infinite strategy needed to achieve it.  Thinking about how to share power while building power is necessary, but it is too much to have to figure out from scratch while doing everything else.  It's as unrealistic to expect every group of people who give a damn to craft tech for mass democracy and distributed power as it is to expect every group of people to make their own word processing and web publishing software .

Which is why we need to build online tools for sharing the power that organizing gives us.

One indispensable piece, unsolved in practice but in theory solvable technically, is **distributing the power of coordinated messaging**.  Coordinated messaging, in turn, is the root of both collective decision-making and coordinating action.

A person who is part of a movement has a piece of information they think every person in the movement should know, or that they think should be spread as widely as possible to the public.  They have have to go through some process that is the same for everybody, such as getting any two people in the movement to approve the content and crafting of the message.  Then the proposed communication goes not to a standing committee but to a small group of people randomly drawn from the movement and asked to decide if it's something everybody should see now, or not.

That's the technology I want to build to make us all going online finally lead to mass democratization and power for all people over their own lives.

Join me.

PUBLISHED: https://agaric.coop/blog/going-online-together-save-our-lives

## Side notes

"services built on the Web and other Internet-based protocols and standards reflect the desire for control": The infrastructure around the internet is also increasingly centralized, which is a problem for building liberating technology that uses the internet but less related to the concentration of power and money in global capitalism.

"worked against many of the tools built on the Internet being peer-to-peer": Our most successful freedom-respecting alternatives, such as Mastodon, tend to be decentralized, rather than fully distributed.
