# A Black conversation on the charter amendment to remove the Minneapolis Police Department as a required department under the mayor's full control

Joi Lewis: What happens if the charter passes, it's not an uncomplicated thing!?

Steve Bulton of the Urban League - wearing Floyd 8:46 hat

Sam

DA Bullock, northside filmmaker extraordinaire

Pastor Brian Herron

Kandace Montgomery

Nekessa, Black Immigrant Collective, she (wearing bold-rimmed pink glasess and a small gray-checked old-fashioned funny hat)


Nekessa: Clearly i speak English and i'm here representing people who do not, but for Black immigrants anti-blackness shows up in those spaces, so it's important that they be represented.


Sam: voices not here, unhoused. Black veterans who know

Dr. Brittany Lewis


Sam: I ran for office in 2017.  Fast forward to i worked in city hall.  The thing that bothers me about charter

all for system changed

that's what i do at

police aimed toward

feel this is a publicity stunt

safety committee, connel and fletcher have very watered-down meetings

never get to timeline and execution

timing is organizers and activists pushing and forcing it

this reminds me of $15.. and waited until after election to take the vote

thinking this is similar cadence

doesn't have teeth for community control at this point, and i'm a proponent for community control

Kandace:  she/them, i'm the director at Black Visions

trying to offer clarity

it's the best next step, it's a step of many

what we know about grassroots change, we can't be involved for just a short amount of time

infrastructure in city to hold new models

we have no way to do that within the city

it's not proposing getting rid of

can we have an office that's concerned with safety and violence prevention

not in this conversation, people living on streets

Black trans women experiencing not

sex workers and other marginalized Black community members

Ida B. Wells and Harient Tubban, Black women who many questioned their leadership


Brian Herron: Not for the charter amendment as it is presently being presented.  I think there's too much confusing.  I agree with Samantha that you have a city council that went from inaction to the most extreme violenec

need transformation not reform

have opportunity to transform it, different mission, different

still have not heard anyone explain what defund the police means and really looks like

City council could have facilitated many conversations about this, and we probably could have arrived somewhere

this is backwards

I have great love with everyone on this panel, we can disagree and still have unity

about liberation and freedom of our people

several organizations, i don't know if they've been invited: Pastor group Elders council, Leslie Redmond, NAACP

Joi: Want to bring DA Bullock to the floor.

Thank you for allowing me to be here.

I am a supporter of the charter amendment for a couple reasons:

Having every Black resident of voting age to have a vote on the charter ourselves, instead of charter commission, established by appointment by a Hennepin county judge, just in pure democracy

Don't often get opportunity to vote on policy or the constitutional framework of our city, opportunity to show up and make our opinion known in that very direct way.

Other reason, without changing the fundamental structure, constitution of the city, we don't get the fundamental

$193.3M/yr explicitly funding just lawe enforcment, not prevention, not intervention

all of us who know how these things in our community know that prevention and intervention

to redo the architecture of how we do these

as we Black people we should know that many of these documents written before we have

Class of 79 Steven Belton
president of Urban League twin cities
i oppose charter amendment but i oppose framing as binary

i know people who oppose defunding but want strong community safety

1.  Doesn't begin to address real problem, institutional racism, white supremacism, and no pathway to ensure white racism won't follow into dept of public safety
2.  Insiduously, [displaces the mayor] replaces one boss with 13 bosses, i defy you [to show where 13 bosses works better than 1]
3.  Take existing police chief out of the running to run this department, i tihnk it's a bad tactical decision

raise up what brian

i think it's OK to decent, OK to be decisive.  We are in same water, in different boats.  What happens though, what


Joi:  I love syncopation, i love jazz.  That's what we drew.

What is the difference between reform and radical change, and why does one instill more

Steve Belton:  Part of the anxiety depends whose listening, you start with form, reform is to redo that.  It assumes the maintenance of the status quo.  Move past

whether talking about reform or radical change, talking about doing things differently, radical change.  Real question, what are people proposing, what is the substance.

Sam: reform implies there is an ability to change something and do better.  The word radical is used to shut down [esp with Black people], basic human right to not be shot down is not radical.

We have a bunch of brilliant Black people who are not elected who work at thi city

chief of police
Sascha Cotton
___, division of race equity exists by charter too, it does nothing

lot of words that don't mean anything, what is the actual plan

when are we going to bring in the other voices

but we'll take something like the Minneapolis Foundation and put them in the middle of the conversation

and organizations
take money that

and foundations, only spending 5% of endowment, putting in money to replace what city is defunding [what when where?]

Racial Equity Committee, DA Bullock used to serve and now i'm co

stop pretending that simply changing the charter

need to deal with basic

starting to become very political with DFL money
what are those outside agendas

[me: these are people

Brian (Tech):  You can throw me under the bus once, but not twice.

DA: We are hearing the consensus that our current system is not reformable, even those who are opposed to this particular charter amendment

it doesn't lead to 'radical' being to the next step

logical is the next step

and that is to fundamentally replace what we have now if it's not reformably

no change since Jamar, Blevins, they put 16 bulletholes in chaser views house this past December, and it went away from our consciousness like it never happened

it's logical to do something totally different

to go back to the charter amendment, it begins there with changing the structural impediment

investing in prevention, intervention

800 officers, plus

that's the way it's weighted out right now, and that's not logical and not reformable

before we even talking about radicalism

we just saw civil unrest that many of us

because we are supporting something that's not reformable and not logical

we need to get together and say what is our next logical step


Nekessa: I think this is also related to the question, whose not at the table, this question of abolition vs reform creates simplistic binary, and we need to be worknig toward expansive political education, outside of academia [etc.]  Many people want abolition but they don't use that language.  They know they don't want the way they have been brutalized and criminalized.

We support the charter being voted on by the public.

We recognize that racism continues in this city, mayor, council, staff engage in ways hat are anti-black

changing law is important, but so much more work to be done

even if it changes into another avenue, under a city council, the police department, the current infrastructure, we still need our eyes on the ball, to push and be aggressive

less interested in words in how this impacts our daily lived experiences

pass charter amendment, and then our next step

go back to community, go back to


What's the plan

i had a police officer partner, i was a civillian, we shut down more

We can't keep talking about

demand city council, county, and state come together and get serious

i don't want crumbs from the table, i want every [agency of government] addressing the disparity, so well document

Kandace: Moving past sound bites, divesting from police is investing in our communities, in all the ways our panelists have been named.

Level of community involvement

charter is just one step of many

this will take entire culture shift about how we engage with safety

we need to be much more deeply engaged, that's what's radical versus


believe in imagination of Black people

from every day

cleaning streets, providing food, community safety systems

part of radical is saying we can develop and transform from this moment at all levels

police is one thing, we need to talk about housing, how do we transform that
work, art, culture

not one policy but changing entire set of systems around that


Dr. Brittany Lewis: clearly angst about city council, ,  and performative nature, also most politics argued current system not reformable

most listed not folks leading organizations, but

Dr. Jordan and i working to bringing people

practical next step

charter creates infrastructure, what does that mean

Sam: Kandace started to hit on it at the end of her

government isn't our only solution

building Black [organizations], whether it's Black Wall Street or what

Black people spent $1.3T spent

and city keeps selling properties for a dollar to white-led organizations

not just charter, not just systems change ecosystem, and building Black.

Brittany: Nothing more to say.  Black infrastructure is key.  What is culture shift, what is culture change?  We need to come together strategically.

Q: Engaging federal forces in defund campagins?

Nekessa: has to be grappled, as we see in Portland, and nothing new, obviously happening before uprising, targets people as terrorists, for activism, for being outspoken, Black muslims.  Police departments collaborate with federal forces, all these secret agreements.

gun databases shared with law enforcemnet

affecting access to housing

state gov sharing COVID data with law enforcement, knowing majority of folks hospitalized, dying, testing positive are Black folks, very concerning

conversation has to be hand in hand

cannot talk about police abolition without FBI, DHS, ICE

defund and dismantle [those] with police abolition

Q: Those opposing charter amendment, what would you propose as next step addressing systemic racism and violence in

"never been any real effort to make any signicant change in policing, any real effort to

i agree with Samantha, this is far beyond just policing

i'd probably feel better about this conversation from the beginning, he has not had

'never been able to discipline'

Brittany: Black and brown police

DA: I'm a northside resident, conversation with chief, dep chief Knight, always wanted, want to have that community member to community member, and power imbalance with that institution when we try to have that conversation

Yes i support you as a Black man who had to fight through a corrupt system

but frankly we're not going to sacrifice ourselves as community members to have a Black chief

substantive conversation, how would he serve community if not serving in capacity as police chief, need to have that conversation with every Black officer that's currently serving, that's part of restoration

I have friends who have officers in Chicago police dept, and what do you view as your role in the community when your job is obsolete?  Because you should be trying to never be wearing that gun again.

Sam: Black people asked to do more with less

have not given Chief Arrondado to h

what's the timeline and deliverables

[names housing and such run by White men]

Joi:  we're not putting a period, we're putting a comma

comments about what we're going to continue to do is build

this is not the beginning but the continuation.  Thank panelists for representing so many

complicated in a good way


[what tools do you want

Black Visions Collective
as well as Black Immigrant Collective

and with that it is right at 8:30 on the dot

please keep sleeves rolled up


Black conversation about mpls charter, may revolution be healing.



***

Dear Ones,

Join us on Monday, July 27th from 7:00 p.m. to 8:30 p.m. Central time for A Black Conversation about the Minneapolis Charter: May the Revolution Be Healing. Black community leaders will explore the multiple perspectives that exist about the City of Minneapolis Charter Amendment. We are all committed to centering Black Liberation and yet we also recognize that we as Black people are not a monolith. We get to engage with each other in principled struggle. There have been many Black community members and organizations engaged in this important work. I, Dr. Joi Lewis, in partnership with Dr. Brittany Lewis am very much looking forward to helping to frame the public conversation around Black Liberation and Radical Imagination as we move toward a healed future. To that end we will have the honor of engaging with Black leaders from both sides of the Amendment at the table in a held conversation with the Black community in a public forum, because Black Liberation must be centered. The “personal is political.” The framework we will use for the conversation is the *Orange Method of Healing Justice (radical self-care + social justice) and radical imagination as core values and practices.

Panelists include: Steven Belton (Urban League), DA Bullock (Northside Filmmaker), Samantha Pree-Gonzalez, Pastor Brian Herron (Zion Baptist Church), Kandace Montgomery (Black Visions), and Nekessa Opti (Black Immigrant Collective).
Event Moderator: Dr. Joi Lewis. The event will be held via Zoom and live streamed through our official Facebook page. It is open for all Black People to register.
Sign-up Form: bit.ly/BlackMPLS
Please complete the sign-up form to RSVP and receive the Zoom link.
All other comrades are welcome to join us via live stream on Facebook. We thank the organizers including Elizer Darris of ACLU and Brian Fullman of Black Barbershops and Congregations Collective, along with all our panelists/organizations, for helping to make this happen. We hope to see you there!
ASL & Somali Interpretation available, please register for more details. 