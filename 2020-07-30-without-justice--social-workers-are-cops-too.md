---
title: Without justice, social workers are cops too
date: 2020-07-30
---

Calling for social workers instead of cops, without also calling for a measure of underlying justice, is just asking social workers to do the job of police.



Let's have equality first.  Then we can see if we need police.  Or social workers.  Or a thousand and one types of nonprofit organizations

Let's also be clear: even the faintest gesture toward economic justice will 'cost' much more than is spent on police and the entire prison industrial complex every year.

Like, we need to get 2% of all wealth redistributed every year to merely match the biblical demand of a jubilee every 50 years.