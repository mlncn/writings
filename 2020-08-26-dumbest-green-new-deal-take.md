There are a lot of dumb takes on Twitter.  But one of the dumbest lately was someone saying a Green New Deal wouldn't move unemployment because we're not a nation of laborers like we were in the 1930s.

He seemed to be arguing contradictory things simultaneously— that public works need 'laborers', presumably manual laborers, and that "we" don't do that kind of work anymore so won't be put to work by the Green New Deal.  If it were true that there were no manual labor jobs at all, maybe they would be just what the 40% of people who are working age but do not have any job at all need and want.  But of course there *are* lots of 'laborer' jobs, and lots of unemployed and unemployed laborers.

The main problem with that particular dumb take, though, was the sheer amount of ignorance embedded in it.  Some correctives:

1.  The New Deal employed all sorts of people, including artists.
2.  The main problem with the New Deal employment programs is they didn't employ enough people and they ended too soon.  World War II did more to put people to work, and we need a much larger effort than *that*.  Saying a Green New Deal won't reduce unemployment because most people now are not laborers is like saying World War II won't reduce unemployment because most people in 1941 were not soldiers— people adapt to society's needs if given a chance, and any one role needs to be supplemented by work spread throughout the whole economy.
3.  If capitalism has encouraged people to develop skills that we have no need for (hint: it has), that's just one more thing we need to fix, even if that means teaching (we need more teachers and trainers too!) advertising executives how to blow insulation into old walls.
4.  But clearly, a Green New Deal at the scale needed to save both human civilization and the environment (hint the environment still has to come first) will need a huge number of technical non-manual work, and even marketers and communication people, if only to drown out dumb takes like that guy's.

***

Published with a couple edits to https://write.as/mlncn/there-are-a-lot-of-dumb-takes-on-twitter

***

Incidentally, if you want to know the 'true' unemployment rate, you need to find the *employment* rate, and subtract that from 100%.  Here's one source repeating the official employment rate for the US: https://tradingeconomics.com/united-states/employment-rate

The labor force participation rate, which i looked at first, is actually the employment rate *plus* the official unemployment rate— the latter being defined only as those actively looking for work.

If you want a measure of how bad things are for people who want to work, you'd need to find a source for 'discouraged' workers, people who do want to work but have given up looking, at least by the government standard of 'looking'.  But i'm not sure how you'd get a really good number for that, even with a deep representative survey.  Would the much larger percentage of women who were housewives or discouraged from even considering having jobs or careers before the 1970s, 1980s, and 1990s count themselves as discouraged job seekers?  Disabled people who are treated as if society doesn't need or want them?  People avoiding interaction with the paid economy as much as possible because it is set up to cater to the wants and whims of rich people, whether it be the economic exploitation in jobs that do help regular people or the jobs serving rich people more directly?
