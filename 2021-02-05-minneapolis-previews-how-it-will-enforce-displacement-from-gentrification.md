---
title: Minneapolis Previews How it Will Enforce Displacement From Gentrification
date: 2021-02-05
---

By: Housed & Unhoused Mutual Defense


Most people do not want to see their neighbors freeze to death.

Erik Hansen is not most people.

