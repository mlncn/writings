---
title: Joining the movement for rent control in Minneapolis
date: 2021-06-13
---

My modification of the letter i signed at:

https://mpls4rentcontrol.org/

### Control rent in Minneapolis

Individuals and households that rent in Minneapolis are most of our city's population, but are being priced out and displaced from our own neighborhoods, communities and homes.

I fully endorse the approach outlined below but i'll note the city has other alternatives:

  - Institute local wealth redistribution with a resident dividend, leaving the median home-owner just as they are but greatly benefiting renters and unhoused people, paid for by absentee and especially corporate landlords.
  - Take over most rental properties by eminent domain and revitalize public housing as true social housing to meet urgent human needs.
  - Accept and support people physically defending their homes, regardless of paper-legal ownership.

If that's too radical for you, better get on this rent control using all tools possible.

We are united to win everything we need to survive and thrive in our own City.  The people of Minneapolis deserve the right to vote on a Rent Control Authorization Charter Amendment this year and you MUST stand with us for:

-Strong Rent Control! Ban out of control rent hikes and price-gouging. We need universal rent control that limits annual rent increases to cost of living (3%).

-No corporate loopholes! Rent control should be applied city-wide, regardless of the size or age of the building, and should not include loopholes like “vacancy decontrol”, which allows landlords to increase rents every time someone moves out.

-Letting our people Vote for the housing solutions we need!  We need all of the possible paths to rent control on the table and on the ballot this year to overcome state's 'pre-emption' that limits the ways rent control can be enacted for a city.
