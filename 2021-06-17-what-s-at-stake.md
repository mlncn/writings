---
title: What's at stake
date: 2021-06-17
---

It has gone 20 days without significant rain.  In a normal June we would have had 20 times the precipitation we've had so far.

We should not consider any of this acceptable.
