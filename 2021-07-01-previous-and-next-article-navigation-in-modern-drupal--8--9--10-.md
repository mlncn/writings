---
title: Previous and next article navigation in modern Drupal (8, 9, 10)
date: 2021-07-01
---

Fun fact: [PreviousNext](https://www.drupal.org/previousnext), an Australian web dev+ firm that's a big player in Drupal, created or maintains 196 Drupal modules— not one of which is a previous/next navigation module, nor do they use one on their blog.
