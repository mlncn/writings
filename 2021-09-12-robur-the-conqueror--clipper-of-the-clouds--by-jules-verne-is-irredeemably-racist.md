---
title: Robur the Conqueror (Clipper of the Clouds) by Jules Verne is irredeemably racist
date: 2021-09-12
---

Robur the Conqueror (Clipper of the Clouds) by Jules Verne is an irredeemably racist book, and it should not be read by anyone except academics studying 19th century racism as reflected in mediocre literature.

I found a 1995 Claremont Classics paperback book that i thought would be light reading as i took a post-vaccine flight ("how did Jules Verne envision flight?")



And yes, i read the whole thing, hoping there would be some subversion of trope, something that might slightly redeem it.


Here is a more generous review of a different, sounds-less-mediocre-and-racist Verne book that at least acknowledges the racist elements head-on:
http://strangehorizons.com/non-fiction/reviews/five-weeks-in-a-balloon-by-jules-verne-translated-by-frederick-paul-walter/


I wrote a bit more and posted here https://www.theglassscientists.com/comic/page-23-12#disqus_thread in response to the direct question!

Yes, unfortunately Jules Verne was very racist, more racist than average, specifically when it comes to anti-Black racism.  At least, Robur the Conqueror (Clipper of the Clouds) written by Jules Verne in 1886 is an irredeemably racist book, and it should not be read by anyone except academics studying 19th century racism as reflected in mediocre literature.  (I found a 1995 Claremont Classics paperback book that i thought would be light reading as i took a rare flight ("how did Jules Verne envision flight?"))


And yes, i read the whole thing, hoping there would be some subversion of trope, something that might slightly redeem it.  Nothing.  Nobody should read it.  Every portrayal of Black people in the US and of Africans went out of its way to be insulting and demeaning.


A French person — 80 years after Haiti defeated France to win liberation for that country and the formerly enslaved people there, 25 years after France abolished slavery in its colonies, mere decades after the US Civil War and the betrayed attempt at Reconstruction, and while France was actively expanding its colonial empire by brutal force — absolutely could know better.


Here is a more generous review of a different, sounds-less-mediocre-and-less-racist Verne book.  Unlike most reviews of Verne, this one at least acknowledges the racist elements head-on: http://strangehorizons.com/non-fiction/reviews/five-weeks-in-a-balloon-by-jules-verne-translated-by-frederick-paul-walter/
