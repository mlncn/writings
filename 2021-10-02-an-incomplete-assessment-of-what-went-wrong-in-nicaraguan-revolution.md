# Dan La Botz offers an incomplete assessment of what went wrong in the Nicaraguan revolution

PUBLISHED: No.

Georgists or <abbr title="Modern Monetary Theory">MMT</a> adherants could claim to have been completely overlooked.  And indeed, while i picked those two economic tendencies at random and in a joking manner, lack of attention 

But a serious examination of anarchist conceptions of democracy is a much more glaring oversight, given La Botz's thesis:

> What alternative was there to the Sandinista organizational model based on their Stalinist Communist origins, the Castro-Guevara examle of guerrilla warfare, and thei own military hierarchy?  Some may argue that only such a centralised, disciplined, and authoritarian organization could have undertaken the struggle against the brutal Somoza dictatorship.  Yet, as we have seen, that model, which failed to build a dynamic interrelation between the revolutionary organisation and the working class and people at large, led to problems which ultimately distorted any struggle for a democratic socialist society.  Among the problems: The Sandinistas' initial failure to ubild a proader party than the small Communist-trained nucleus with which they began; their failure (with a couple of important exceptions) to build an underground industrial labour union and peasant union movement; their failure to lesiten to the peasantry and to turn land over to the peasants themselves, a key failing which led to the growth of the Contras; their initial failure to work out an autonomy agreement with the indigenous people, which also contributed to the grownh of the Contras; and finally, the failure even after the victory of the revolution to call a convention and create a democratic organisation, the result of which was the concentration of power in a handful of leaders accompanied by personal self-aggrandisement and self-enrichment.

After this list, to suggest the great lack was some kind of commitment to party-based democracy is deeply unsatisfying.

models of organizing that incorporate direct democracy 


Sandino, the namesake for the Sandinistas, favored cooperatives, one of his only political or economic policies of note.

First, let me say that the book serves as an excellent introduction to Nicaraguan history in general and, in particular, the generational battle against the Somozo dynasty and the aftermath of the successful revolution.  The organization of the book leads to a fair amount of repitition, but the most important facts bear repeating.
