# Politics needs non-electoral organizing

PUBLISHED: No.

If you are a person who gives a damn, and you get caught up in electoral politics, somewhere in the back or front of your mind you surely know elections are woefully insufficient to achieve our liberation.

We need on-the-ground organizing to directly meet our needs.




------

DSA and WDA collaborate on canvassing database

specific info on each person can only be obtained by going to one of three people that holds that info, and each only holds at most 20 people's info (so we have an 'organizer to organized' ratio of at worst 1:20)

this can be done with a blockchain if people really want to.
