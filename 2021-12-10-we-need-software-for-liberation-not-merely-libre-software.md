---
title: We need software for liberation not merely libre software
date: 2021-12-10
---

> I care for protecting users - especially from vulnerable groups due to some other means of oppression. A world I would love to live in is a world of justice and access to the basic necessities for all, without exception. In an incisively digitized world, the means of communications, socialization, citizenship, education and creation are technological means. Those digital means I advocate should be universal, and even if I come off as extremist, an extension of Human Rights. If a legal document, such as the GPL license is one pathway to achieving digital justice and universal computing programs, then yes, all software ought to be free software! But if free and open source becomes a goal rather than merely a method, then we will get stuck under nonsensical knots.

From https://alex-esc.github.io/posts/new-mastodon-todon.html about leaving Librem Social.

More on the Nazi-friendly policy of Purism's Librem Social: https://anarc.at/blog/2019-05-13-free-speech/

See also / remix into series with: https://gitlab.com/mlncn/presentations/blob/master/free-software-and-cooperative-platforms/full.md