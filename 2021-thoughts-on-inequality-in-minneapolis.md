# Some thoughts on inequality in Minneapolis

(Written November 2021 by hand on a little pad.)

 * i see by the actions of the police and courts, by the words of landlords and business owners, that people trying to survive outside – outdoors and in part outside economic activity – are a direct challenge to capitalism.
 * there is no wealth created in the past 500 years that is not compromised by – largely comprised of – land theft from and genocide of ingidenous people, enslavement of Black people, destruction of the global environment, and the ongoing exploitation of inequality that is intertwined with all of that.

plus massacres against labor, and ongoing oppression which is today typified by the prison industrial complex (PIC).

Every non-rich person is owed reparations.

The residents of the Near North Camp, which is majority Black and Indigenous at any given time, especially are owed reparations.

The demand to be outright given the land is a moderate one.  Five miles away the city is giving $__ million worth of property to the wealthiest family in the state [fact check!].  Giving an estimated &__ mil of vacant land to 25 of the least wealth is reasonable.

We should be occupying buildings, siphoning from electric or gas lines instead of buying propane, growing food, shaking down Target for necessities.

[insert name of woman who ran for and won election to the Parks Board] repeatedly said five unhoused people died in parks so camping must not be allowed there.  At no point did she articulate why her beloved parks were particularly deadly to unhoused people, so she is clearly saying "Go die somewhere else."  Her calousness was rewarded with election.


We are the 99%, but a solid chunk of that desperately wants to pretend their fortunes will be tied to the oligarchic 1% and not the literal man in the street they have materially so much more in common with.  [rewrite or remove]

There's so much vacant land in Minneapolis alone, why don't police and the rich who direct them just ignore [unhoused people living on out-of-the-way-spots]?

Stated reasons don't stand up to scrutiny [("it's for protection of the people living outside")], they let us die or kill us outright all the time.  I think they, and cities across the country, fear shantitowns would cover half the city soon.  And i think they're right.  People would opt for homelessness over capitalism.

Sidebar: If they hassle unhoused people, they're a cop— so transit police and especially parks police; sherrif department and state police cannot be forgotten just because MPD gets the headlines for murder. (the missing headline for murder is the Hennepin and Rmsey deputies in federal task force killing Winston Smith) 




