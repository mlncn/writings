https://opencollective.com/foundation/updates/open-call-for-ocf-board-members-ocf
https://docs.google.com/forms/d/e/1FAIpQLSckC8iY8tp5D4wDFytIxsNj_qEX6YUmKpjZa3YiMjM9h2hnlw/viewform

OCF Board Member - Expression of Interest
📣. We are currently searching for 1 or 2 people to join the Board of Open Collective Foundation (OCF). 📣

Open Collective Foundation is a nonprofit with a tech platform that enables over 300 groups to raise and spend over $13 million dollars a year in full transparency. We are 1 node in an international network of 600 non-profits, LLCs, and co-ops around the world that use the same, open source software to create efficiencies with money. 

✨✨ More information about the call for Board Members is here: https://opencollective.com/foundation/updates/open-call-for-ocf-board-members-ocf

💻   Website: https://opencollective.foundation/
🛠.  Docs: https://docs.opencollective.foundation/
🍃   Monthly Community Forum: https://opencollective.com/foundation/events

Twitter: https://twitter.com/o_c_foundation
Instagram: https://www.instagram.com/opencollectivefoundation/
Facebook: https://www.facebook.com/ocfshares/ 
Slack: https://opencollective.slack.com/join

✅.  Please fill out this Expression of Interest between now and March 21st. Our team will interview shortlisted candidates and invite selected directors to join the board in the summer of 2022 for a 2-3 year term.
Sign in to Google to save your progress. Learn more
* Required
First Name *
Last Name *
Email *
Pronouns *
URL: LinkedIn Profile or Website
In 3-5 sentences, tell us: Why are you excited about the prospect of joining the Board of OCF?
Open Collective Foundation explained in 2 minutes:
Check which of these describe you. You are...
Based in the US and experienced with the 501(c)(3) nonprofit structure.
excited about Open Collective Foundation (OCF)—especially if you’re involved in one of our hosted initiatives!
skilled in one or more of the following areas: law, HR, funder collaboratives, fiscal sponsorship, guiding nonprofit organizations with $5-10M operating budgets that are experiencing rapid growth.
a champion of transparency, participation, solidarity, and social impact.
actively working to spread power and wealth and root it in community, e.g. participatory budgeting, mutual aid, open source software, #landback, bail funds, strike funds, giving circles — in line with our strategy of Solidarity as a Guiding Principle.
excited about our cutting-edge tech platform (OCF is 1 node in an international network of 600 non-profits, LLCs, and co-ops around the world that use the same, open source software to create efficiencies with money).
ready to participate in and help us grow collective governance, both in respect to facilitating more participation from our hosted initiatives in aspects of governance, and preparing for OCF to participate in governance of the OC platform (which is aiming to Exit to Community).
available 5 hours a month for a 2-3 year term (see the Board “Commitment” section in the OCF update).
comfortable supporting a remote, online team that is both collaborative and autonomous.
Other:

Optional: What regions of the US / Turtle Island do you have connections to?

[ ] North West
[ ] West
[ ] South West
[x] Mid-West
[ ] South East
[x] Mid-Atlantic
[x] North East
[ ] Guam, Puerto Rico, American Samoa, the U.S. Virgin Islands or the Northern Mariana Islands
[ ] Other:

How did you hear about this opportunity? *

Solidarity Economy Network [SEN] e-mail list message by Mike Strode is where i noticed it first, despite being so plugged in to Open Collective and Open Collective Foundation updates directly!
