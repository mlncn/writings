I thought the high deductible health plan would limit my financial risk to at most the cost of premiums plus the deductible, so in the area of $12,000 a year at the most expensive no matter what happened to me.  But it looks like there is no limit to the cost of out-of-network care, which basically means if i have a medical emergency—especially out of state—my costs could be infinite.  That is not acceptable insurance!

Also, i expected preventative care to be free and encouraged.  There has been no help getting a primary care doctor (if that is indeed the first step) or recommendations to go to clinics.

Hell we have not even been told about eligibility for additional COVID-19 vaccine boosters (or initial vaccination, i don't think UCare knows my status).
