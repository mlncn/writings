

Letter sent to https://opencollective.com/mpls-northside-mutual-aid/

> Volunteer opportunities
> 
> Hello, I’m interested in helping to the best of my ability with meeting
> the material conditions of those in north Minneapolis, and want to help
> out with what u guys r doing

i replied:

That's wonderful.  Do you have any particular interests or capabilities or resources?  There is simply so much!

   - access to a truck/hatchback?  water barrel delivery!
   - access to credit card and ability to go into a store to make payment? Getting people phones (and being reimbursed).
   - organizer?  elicit needs, encourage community meetings within camp and possibly with neighbors.
   - writer?  updates to donors and/or representing the history of the encampment and the lack of options forcing people to stay there.
   - helping people plug into specific times and places to help!

Or lots of other things, that was just off the top of my head.  Food coordination needs more energy.

Also wanted to let you know about a brand new, separate effort:
https://www.instagram.com/northsidemutualaid/

In solidarity,

benjamin

