Hi Jeremiah, and hello Bethany and Dieu!

First, if you are not caught up on the brutality with which the city is destroying encampments—rather than housing people, even temporarily as a way to get people to leave an encampment voluntarily—please let's schedule a time to get caught up, with people directly affected and the professionals who give a damn.

Not optimistic about policy that does not further criminalize poverty coming out of this council with the mayor's veto, but public attention can stop the worst of the abuses.  And proposing policies can help bring that attention.  I would like to see simple, universal policies, such as "No city resources will be used to support the involuntary removal of someone from their home."

Back to the matter immediately at hand:

One entitled aspiring fascist can call up 311 or whatever and have someone else's *vehicle* that is not harming anyone stolen, presumably by private towing companies working for the city, overnight.

Traffic control officer wrote up a ticket, at 9pm, for a car that's visibly in the process of being repaired.

Parked on the side of the road like other cars that may sit there for months with no hassle from the city.

"It needs at least three tires", traffic control cop said.

The tow truck can come as soon as four hours from the ticket, he said.

And sure enough goddammit it was gone by 5am this morning.

Minneapolis will cause so much harm for no benefit.

If there is *any* technicality, any violation of policy, that can get this person their car back, money refunded, and an apology— that would be ideal and would give a person who likely, rightly, has no trust in government matters a reason to be civically engaged.

Not sure why i am always surprised that there are more ways to punish people primarily for poverty.

What can be done policy-wise to reduce the use of city enforcement as a weapon of misguided gentrifiers?  At minimum, something nice and general that makes clear that personal property which is not impeding firetrucks, ambulances, or buses will not be seized without at least a month for it to be arranged to be moved.

Or take a page from the conservatives playbook and require that the public benefit outweigh the private cost.  This car was in no material way different from any other parked car, and the, what, $500+ hit to get it back (fine, towing fee, storage fines, plus towing back?) could actually ruins someone's life and make them miss work and/or rent and end up losing housing and be the next person to have their remaining possessions destroyed by the city's cruel (lack of) policy toward unhoused people.

Fines are a major regressive tax and the weaponization of city ordinances by people who feel entitled to have the city at their beck and call makes it worse.  This harmless car under repair gets removed in HOURS while people here have been fighting to stop the extreme harm of pollution from Northern Metals and the incinerator and the highway for DECADES?  Sorry, the injustice makes me rage.

Please also start introducing policy proposals to eliminate flat fees.  Flat fees are a regressive tax.  If rich people want the city doing graffiti removal, fine, have it come out of property taxes, but it is unconscionable that this—and other costs of staying alive, like the base water and sanitation fees—are charged regardless of ability to pay.

How do we ensure Minneapolis is a place that the people who live here can afford to live here, and even own homes here?

Increase the property tax to 12% and give every cent of that back to people—an equal amount per person—as a cash rebate.

You live in an average home?  Your effective tax rate does not change.

You live in a mansion and own forty properties?  Thanks, your taxes are funding rebates to help people survive what capitalism has done to our communities and our planet.

You live out of your car?  Have $12,000 a year to use as you wish.

… i've gone a bit off-topic but point is the status quo is vile and pretending that we don't need drastic action to address inequality is how we end up with liberals rationalizing ever more punitive and carceral policies toward anyone less fortunate.

Please help.  In communications if not in policy.  Thanks.

Your constituent,

ben

-- 
https://agaric.coop
+1 508 737 0582

P.S.  Why didn't you tell me unhoused people can vote?  It's admirable that you have been down to talk to and support people you don't expect to vote, but wow we could organize big vote-and-get-paperwork-in-order days!

https://www.sos.state.mn.us/elections-voting/register-to-vote/im-homeless/
