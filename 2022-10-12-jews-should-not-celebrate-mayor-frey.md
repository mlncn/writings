

having his police order the tearing down of tents and open structures *on sukkot*.

I don't need to spell out why collective punishment and "following orders" are matters of intergenerational trauma for us.  ("every 100 years" twitter thread)

Temple Beth Isreal in downtown Minneapolis is proudly spending nearly half a billion dollars a year to partner with Minneapolis police and it sounds like our Muslim-harassing, Constitution-free-zone fusion center and FBI.




******

criminal injustice system / shelter background

(responding to "did Avivo really house 11 people yesterday i don't believe it")

that's the word from people at the protest encampment, too.  I helped get one person there last minute so wouldn't be surprised if it is now the full 12 they offered.

Do note that 1) doing this after violent, traumatic evictions where most people lost nearly everything they owned, when it was very much NOT clearly and affirmatively offered before then, is still pretty awful and sort of gaslighting.  (And as Mike notes probably kept other people from getting this housing; for August Avivo tiny village reported 93/100 spots filled).

And 2) for the most part only people who are not entangled with the criminal injustice system were willing to set up in surveillance city (and so easier to place / more willing to engage with government-contracted carceral-adjacent organizations).  Around 10% of people in the US are entangled in the criminal injustice system— for example around 2% of the US population has outstanding warrants and 7% are under correctional supervision.  Among the criminalized populations of Black, Indigenous, poor, unhoused etc the proportion is of course higher.

Is there / should we push to get a statement that Avivo and other or future transitional housing that they will not collect/report people's info to police nor discriminate based on judicial system status?


******


Exactly one week ago, MPD and Public Works rolled in 100 deep, announced five minutes to pack, and tore down our home.


******

The minimum restorative justice would be that the houses of Jacob Frey, Peter Ebnet, Andrea Brennan, Erik Hansen, Elfric Porte, [police execs], Saray, [public works execs], [parks execs], [county execs], (ideally, every single person's house would be linked on Zillow, or on a paper flyer with $$ and address) be turned over to the unhoused people they have relentlessly and cruelly targeted for more than two years.

None of these people would become homeless— not on the street.  All have the savings, rich friends and family, and/or additional goddamn houses to fall back on.

Likely none would have to join the ranks of the [housing insecure? homeless but staying on others' couches?  what's the term? with over 18s living at home we're talking a third the population]

******

Cover lot signs with stickers: short slogan plus tipline number and short domain like:
 - stolenland
 - FreedomToLive
 - ResidentDividend
 - LotsForAll
 - HomesHereNow
 - UnhousedRights
 - DefendNeighbors
 - ProtectPeopleNotProperty
 - PeopleOverProperty (literally)
 - LandMisuse (is People Abuse)

******

Policing reproduces the conditions that make many people demand policing.

This is almost always the case— tearing apart families, increasing stress, stealing money through civil asset forfeiture and impound and fees and fines, destroying resources and community 

This is never so clear as when police assemble to perform, in its purest form, their true function: repression.

Metro Transit police spend most of their time taking enforcement action against "crimes" of poverty— panhandling, sleeping on the train, living your life in public simply because you don't have the same access to private space, and—of course—fare evasion.

Then, on the rare occasion people directly demand something to mitigate poverty, such as housing, Metro Transit police fully do their part to suppress any peaceful petition to the government for a redress of grievances.


******

"The city ass holes really are heartless. I got it set up and insulated perfectly to where I could keep warm with one candle, now I gotta up and move"

******

Picture capitalism as a series of conveyer belt assembly lines, but the people have to run on them to even stay in place.  The poorest people are at the end, on the conveyer belt that is running the fastest and is the most crowded.  If you cannot keep up or you get jostled too hard, you fall off.

(rats, metaphor sort of breaks down here)

If you find you still have something to stand on, and others see that, people are likely to jump off.  The worst conveyer belt stops moving.  The second-worst one ahead of that slows down a bit, as 

The only way to keep everyone on the treadmills is to make falling or stepping off *worse* than simply being unhoused and relying on support from others.

If, on the contrary, people were to receive actual housing— a chair, a couch, a room in this contrived conveyer belt factory metaphor— many people would stop or slow down, demand better working conditions, demand more pay.

To prevent rapidly rising working and living standards for everyone, and a re-focusing of work on what needs to be done and what people feel fulfilled in doing, and massive reductions in profits for the owners and the reductions in the privileges of those operating the conveyer belts, to prevent everything getting better for the majority and to prevent those in power from having less power, governments cannot allow people to be well-taken care of, or even visibly OK.

To see all over a city, as when the sanctuary movement took to the parks, that people will take care of each other without government, that mutual aid can provide when corporations withhold, would devastate the capitalist system, as people learn they can take at least a breather from it if necessary and be all right.

So in Minneapolis, it does not matter how out-of-the-way an encampment is, how unused and abandoned the land it is on was, or even how low profile individual campers try to stay— anything short of enforcing total social disappearance of people 'outside the system' is a threat that must be eliminated with as much force as necessary.
