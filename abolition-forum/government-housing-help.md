Full list of "project types" that HMIS considers:


export const state = () => ({
  list: [],
  structure: { assistance: { homeless: { unit: "Days", included: true }, housed: { unit: "Days" } } },
  types: [
    { name: "Emergency Shelter", homeless: true, housed: false },
    { name: "Safe Haven", homeless: true, housed: false },
    { name: "Transitional Housing", homeless: true, housed: false },
    { name: "Rapid Rehousing", homeless: true, housed: true },
    { name: "Permanent Supportive Housing", homeless: true, housed: true },
    { name: "Permanent Housing Only", homeless: true, housed: true },
    { name: "Permanent Housing with Services", homeless: true, housed: true },
    { name: "Coordinated Entry", homeless: true, housed: true },
    { name: "Street Outreach", homeless: true, housed: false },
    { name: "Prevention", homeless: false, housed: true },
    { name: "Services Only", homeless: true, housed: true },
    { name: "Not an HMIS Project Type", homeless: true, housed: true }
  ],
  unitTable: { Days: 1, Months: 30, Years: 365 }
});
