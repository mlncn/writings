Distributed models of the web are analogous to anarchist federation.

no person should have power over another person.

every person should have the most power possible over their own lives.

there's a potential tension here

left completely alone, no other person has power over another, but we don't survive that way

together, we can accomplish a great deal, but so many structures of working and living together are oppressive and exploitative.

autonomy, and in much anarchist (and other) thought, autonomous groups are a way to strike a balance.

this same tension exists on the internet.  We can accomplish more together.  How do we preserve individual autonomy while doing so?

each of us could in theory host our own web presence, but that's time-consuming and takes knowledge and keeping up with technology changes and security threats is ultimately difficult.

the federated model, currently epitomized by Mastodon online, 

but the anarchist ideal is not that people get to choose their ruler.  It is, indeed, no masters at all.
