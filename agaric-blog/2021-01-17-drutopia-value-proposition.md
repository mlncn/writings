The basic idea is to give groups a $50,000 website for $500 per year.

That's a harder sell than you'd think, when a grassroots group would never imagine spending that fifty grand in the first place.
