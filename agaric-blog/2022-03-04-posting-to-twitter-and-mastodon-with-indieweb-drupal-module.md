# Posting to Twitter and Mastodon with the Indieweb Drupal module

Essential for being in control of your online life, post somewhere you have control and cross-post anywhere else you want people to hear you.

## The markup

The first step is to add a little bit of markup to the pages you want to be able to share— of course, if you are using a content management system like Drupal, or even a static site generator, 

[h-entry](https://microformats.org/wiki/h-entry)

This helps your website talk to other websites, in ways that are easy to understand implement in any web technology, [h-entry](https://microformats.org/wiki/h-entry)

## The mention

To help us all achieve this digital independence,  [Indieweb](https://indieweb.org/) is a movement, a set of approaches, and – conveniently – a [Drupal module](https://www.drupal.org/project/indieweb) to bring in your Drupal or Drutopia site!

Agaric has our account at [social.coop](https://social.coop/).  (Note:  As is typical of cooperatives, financial participation is required, with a recurring contribution via Open Collective, about $17/year).

If you want to get started immediately, no cost and no approval period, [mastodon.social](https://mastodon.social), the big open instance run by the Mastodon developers, is the best option.


https://brid.gy/mastodon/start

Where it says "Enter your server", put in the Mastodon server that the account you are using 

You are not done yet!

You must also press the Mastodon button on the right side of the page that says "Click to enable publishing: mastodon", where "mastodon" is an image including the Mastodon logo and wordmark, and that is what you need to click.  (The word "publishing" is a link but it takes you to more information, it is not what you click to enable publishing.)

Once it is connected, add it to IndieWeb's Webmention Sending Syndication Targets, at `/admin/config/services/indieweb/send` like this:

```text
social.coop (bridgy)|https://brid.gy/publish/mastodon|1
```

The first part is simply a label, and it will appear on content under a "**Publish to**" section added to the edit form of pieces of content for people in roles with the "Send webmention" permission, which accurately describes itself:

> Allow access to the "Publish to" component to publish content to other sites.


Now, go to the manage display tab for each content type you will be pushing to Mastodon or Twitter.  For articles, for example, `/admin/structure/types/manage/article/display`.

(If you do not see your syndication target, with the same label you gave it (so "social.coop (bridgy)" in our example above.

Move "Syndication" and all fields corresponding


