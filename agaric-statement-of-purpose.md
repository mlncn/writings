# A renewed Agaric Statement of Purpose

Most people want good things, for themselves and everyone else.  We want health, happiness, and freedom.  We instinctively favor fairness.

That we have so little to go around, of all this and more, is less a people problem than a systems problem.

We want to build tools that help people transform systems to serve 

build power while sharing power

Help people gain power over their lives while building ways to share power


