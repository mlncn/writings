# Do not say "Judeo-Christian"; say Abrahamic or People of the Book

I've felt this way for years but finally put a finger on why it bugs me so personally.

It's recruiting my religion, my people, into a horrific war i want no part of.

The phrase became ubiquitous in the first decade of the twenty-first century in essays written by highly-educated, well-paid white men positing the existence of a "clash of civilizations" as if that very framing were not stupid and dangerous.

***

Thank you for your work, and for the [thoughtful essay on idols](https://www.quixote.org/statues-stories-and-american-idols/)! Yours is a needed perspective, holding up the true tradition of Cervantes.  One request, though.

Please do not use the term "Judeo-Christian".  Its exclusion of Islam makes
it an insidious and harmful propagandic phrase.  It has not had an accurate
meaning at least for the past 1,400 years, and i am quite confident it was
never used before then!

"The Judeo-Christian tradition" can be replaced with "Abrahamic religious
traditions" easily and more accurately in this essay.  The topic is a
perfect example of the inaccuracy of the common term, since the prohibition
on graven images and idol worship is honored much more strongly in Islamic
traditions, on the whole, than in Christian traditions.

"People of the Book", the Islamic term which is inclusive of Jews,
Christians, and Muslims is another good alternative.  This designation has
already had very real consequences, making Muslim rule of the Iberian
peninsula a golden age for Jews in Europe.

I feel a cultural or religious scholar could give a better term for some
pre-Judaic movement against worshiping images, or for that tendency across
religions, but i don't know it and am certainly not asking for that level of
research in a piece written for a general audience.  Just don't
unintentionally and inexplicably exclude the religion of a quarter of the
world's population from their own religious traditions!

Sincerely,

a Jew with a recollection of the United States government's current penchant for killing Muslims and knowledge of some bits of world history.

***

(this was an e-mail to Quixote Center and i still need to adapt it to be general, including making the point that dividing by religion period is probably already a warning sign)