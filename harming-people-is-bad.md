# Harming People is Bad: Situating Crime in US Society




How many murders take place in prison?

A year in prison takes two years off a person's life expectancy.  A person sent to prison for ten years doesn't have just that decade stolen from them, but **thirty years**.


In some important ways, Sharkey's book is very thoughtful.  In others...  like really there needs to be a consistent definition of crime, at the start he compares the most 

It is interesting that more police officers in the street == reduced crime



It's like, your country is flooding instead of building a dike you detonate atomic bombs and through directly vaporizing and boiling off, you are able at continued great expense to reduce water levels by 20%.  Like, you can't argue with the fact that it's technically working, but it's clear from thinking about it and imagining other ways, or just looking at other places or other times, that there must be a better way that isn't causing more harm than it's preventing.


***

On page 99, Patrick Sharkey addresses rising inequality:

> The decline in violence may have saved thousands of lives, and created calm classrooms and safe parks, but the question remains: Has it given children a better chance in life and allowed them to rise up out of poverty? Has it had any impact on inequality, or has the long-term fall of violent crime simply made cities more comfortable for the new urban elites?

The set up of the question worries me.  It's nice to acknowledge poverty and inequality as unequivocally bad, just as we can recognize murder and rape as unequivocally bad.  But "this bad thing got worse without preventing this other bad thing from getting less bad" is not the most gripping analysis.  Far more interesting would be looking at the ways one bad thing (inequality), or protecting that bad thing, *constrained* the available responses to the other bad thing (violence).
