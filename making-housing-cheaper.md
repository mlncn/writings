I cannot speak for these particular solutions.  I do know that many people who are unhoused are very interested in all sorts of housing solutions— from tiny houses to cargo container to modular —while the skills i have seen employed tend toward wood-frame construction of varying kinds, and of course robust tents, sometimes nested on platforms, and keeping RVs habitable.

The point is that we all know housing costs are the issue, but almost no one talks about bringing down the cost of building a house, *except* unhoused people (shipping container modular houses ordered direct from China, being one idea for example, and the actual structures everyone builds to live in being the most real examples!)

Most of the reason is that economic stability for rich and middle-class people is highly dependent on propping up housing as an investment.  Lowering housing costs hurts rich people, helps poor people, so ideas like this don't make it far. 

https://twitter.com/AnRacMedia/status/1543703298900910080
