
In recent news of technology helping humans be inhuman to other humans, Google touted a feature in a new phone of having a virtual assistant answer your calls for you and use artificial intelligence to screen out (presumably politely hang up on) spam callers.

This is wonderful until you take a step back and remember that the only reason we need something to screen out spam calls is because of the automation of spam calls in the first place.  And hardly anyone thinks they should be legal.

Unfortunately, "AI arms race" isn't just a metaphor.  Undoubtedly, there are horrific and ethically unacceptable AI
