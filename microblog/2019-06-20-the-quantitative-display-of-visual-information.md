title: The Quantitative Display of Visual Information
date: 2019-06-20
---

This typographically impeccable blog post will just be a very nicely formatted table of the number of each type of chart/diagram/etc in Edward Tufte's book, "The Visual Display of Quantitative Information".

OK, hilariously, a ton of resources accidentally reverse the title the same way, "The Quantitative Display of Visual Information".
