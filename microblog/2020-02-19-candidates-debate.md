Human rights had a good evening at the debate.

Amazing to see on national TV talk of a wealth tax (aside from being needed to bring a modicum of justice, it is much healthier, in terms of economic incentives, than income tax) from Bernie Sanders and Elizabeth Warren.

And Amy Klobuchar said something about a dividend to people dealing with global warming and the pollution of others, who should pay?!?  Yes please.

But a big missed opportunity to get another key topic on the national agenda— fighting back against the spurious sanctity of contracts.  Human rights are always more importan than the idea that anything put in an agreement must be respected and enforced by the government.

Bloomberg's draconian non-disclosure agreements (NDAs) with thousands of employees, and dozens of women he has reportedly mistreated, gave the opening.  Just as 115 years ago it was wrong of the US Supreme Court, in Lochner v. New York, to hold that a contract between two (unequal) parties overrode public interest legislation (to not require people to work more than ten hours a day), it is wrong today for an NDA to stop someone from speaking out about sexual assault, it is wrong for contracts that may come with every service you buy to take away your right to sue when your rights to life, liberty, and health are violated.

***

https://en.wikipedia.org/wiki/Lochner_v._New_York

https://www.salon.com/2017/07/24/a-21st-century-form-of-indentured-servitude-has-already-penetrated-deep-into-the-american-heartland_partner/

***
Yes, redistribution of stolen resources 
