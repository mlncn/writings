A European writer considers himsef to be part of an old and onorable tradition—of intellectual activity, of letters—and his choice of vocation does not cause him any uneasy wonder as to whether or not it will cost him all his friends.  But this tradition does not exist in America.

On the contracy, we have a very deep-seated distrust of real intellectual effort (probably because we suspect that it will destroy, as I hope it does, that myth of America to which we cling so desperately).

  — James Baldwn, Nobody Knows My Name (more notes of a native son), 1961, pages 19–20 (in the 1963 Dell paperback printing my mother has).
