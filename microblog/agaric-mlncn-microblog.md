## 2022-04-29

Hey @cloudflare your fake "Processing…" JavaScript counter has kept me engaged for 23 hours but i don't think your Speed Test Tool is currently working

[screenshot started  minutes and  seconds ago]

## 2018-09-30

Sir Tim Berners-Lee has [turned to venture capitalists](https://www.fastcompany.com/90243936/exclusive-tim-berners-lee-tells-us-his-radical-new-plan-to-upend-the-world-wide-web) to fund his vision for a web that benefits billions of people.

It's a searing indictment of our current economic system and the opportunities it affords that a person with so many institutional and personal connections and such opportunity to have his voice amplified felt the need to do this.

A fully functioning economy would afford the people directly affected and the most passionate supporters the resources and access to institutional capacity to directly aid the 



## 2018-09-30

I hope this explains Berners-Lee's [betrayal](https://www.defectivebydesign.org/blog/response_tim_bernerslees_defeatist_post_about_drm_web_standards) of [a web that's for the people using it first](https://www.techdirt.com/articles/20170707/15544137737/tim-berners-lee-sells-out-his-creation-officially-supports-drm-html.shtml).  If you're going to take on Facebook and Google, you're going to want some big money interested in seeing you succeed.

I still disagree with it but it's a better—more hopeful—explanation than cluelessness or having spent so much time with lobbyists for the Motion Picture Association of America and Netflix that he actually thinks like them.

## 2018-11-04

Drutopia will mostly avoid this problem by not having an enterprise sales team...  but there's still good advice in here for balancing having a product (a software service that everyone can use) and servicing particular clients by building features just for them.  https://www.mironov.com/sales-led/


## 2018-11-22

We know we have a lot to offer.  Corporations pay big money to get a little collaboration and broadbased participation; IdeaScale ("an idea management platform that uses crowdsourcing to help you find and develop the next big thing") has a "Lite" plan costing five thousand dollars a year.

For us, cooperation is built into our structure.

Yet [alternativeTo reports no Open Source alternatives to IdeaScale found](https://alternativeto.net/software/ideascale/?license=opensource).

What are the ways that we do the following?  (Ripped from IdeaScale.)

* Let your network share feedback and ideas from anywhere. You’ll find answers to questions you never even thought to ask.
* Collaborate to evaluate, enhance, and prioritize the ideas best suited for implementation.
* Extract insights and crowd wisdom from ideas and then refined in order to populate a sustainable innovation pipeline.

As a cooperative, we could purchase and provide IdeaScale's services...  but i don't even think it's approach is the best.  The mechanics it uses emphasize voting 
