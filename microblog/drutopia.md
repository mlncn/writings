2018-12-04

Really, we need to do better. The structures of the industries Standard Oil, AT&T, IBM, and Microsoft operated in remain the same after anti-trust action. Monopolistic and oligopolistic abuses of power continue. Cooperative platforms would put the most-affected people in charge.

(in response to Mar Hicks)
https://twitter.com/histoftech/status/1069838314696515585

"Technology platforms are privately controlled infrastructure, the underlying backbone for much of our economic, social, & political life." We need to deal w/them just like past oil, transport, telecoms & computing giants--from SO to AT&T, IBM & Microsoft.:

https://bostonreview.net/class-inequality/k-sabeel-rahman-monopoly-men
