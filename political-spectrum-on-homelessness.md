The political spectrum on homelessness:

  - Reactionary: do nothing to help people keep or get housing, and violently attack
  - Conservative: do nothing to help people keep or get housing, unless first satisfying prejudice by imposing the 
  - True/thoughtful moderate: Give people housing, cooperative or otherwise, and increase their ownership stake the longer they take care of it
  - Liberal: do a little to help people keep or get housing
  - Radical: Eliminate private property for anything largescale, de-commodify housing, socialize production and give it to people, cooperative or otherwise


As a society, as represented by our governments, we are somewhere between the reactionary and the conservative response, and doing more of the reactionary stuff all the time.

If you, like the vast majority of people, want to move us toward any of the other positions, you need to make common cause at some level with people in the other camps— especially radicals, who are as always doing the heavy lifting toward getting us to a better place.
