# Turning the burdens of bureaucracy we bear individually into a basis for collective liberation

Most people in the US have been wronged in some of these ways, adding up to billions of dollars taken from us:

- [wage theft](https://pwgd.org/knowledge/wage-theft)
- supermarket overcharges
- bank fees
- security deposits kept or services denied by landlords
- available but hard to access [government benefits](https://pwgd.org/knowledge/government-benefits) & tax credits 
- telecommunication services at ratcheted up prices
- [arbitrary decisions by online platforms](://pwgd.org/knowledge/arbitrary-decisions-by-online-platforms)
- state takings of 'abandoned' assets
- incorrect or biased credit ratings
- 'right to be forgotten' or anonymous for ordinary people ([Crash Override](http://www.crashoverridenetwork.com/index.html) help, expunging or making harder to find criminal records)
- larger entities owing payments to smaller and never paying

People Who Give a Damn, Inc. (PWGD) intends to pilot a project to help people reclaim what is due to them.  People mistreated in the past can purchase a semblance of justice by donating to help people battling bureaucracy now.  Anyone, affected or not, can join in to help. In addition to paperwork and calls with customer service, we leave our options open to:

- connect people to take actions in solidarity with one another
- make public records and FOIA requests
- mount public pressure campaigns, online and in the streets with pickets
- organize sufficient numbers of people with the same need to meet it in a different way, even forming cooperatives to provide it for ourselves
- gather evidence for legal proceedings (civil or by attorneys general, but PWGD itself would not be bringing suits)
- send in official copies of contracts with unacceptable portions modified

If successful, this work will itself live in a large cooperative including people in their roles as both customers and workers.  Through limited [power of attorney](https://www.mncourts.gov/Help-Topics/Power-of-Attorney.aspx) agreements (which do not require the helper to be a laywer!), paid or volunteer workers will take on tasks that anyone could in theory do on their own, but often do not for reasons of insufficient time, knowledge, or emotional stamina.

Non-confidential details of each interaction with an institution will be made public.  This makes it easier for people to take independent action to pressure corporations and governments to do better.  It also supplements and adds depth to another part of the initiative, to document the steps for navigating broken-by-design bureaucracies, [IFixIt.com](https://ifixit.com/)-style.

In every case, if some group is already doing (or documenting) a recuperation well, we should partner with, lift up, and financially support them.  What we do that may be unique is bring people together in communities of interest where [communication is controlled by the people in the community](https://pwgd.org/community-moderated-mass-communication).

The more people we have in a particular situation, the more we can do a form of collective bargaining.  There's certainly a risk that the more clearly we are building power, the more companies (and governments) will oppose our efforts.  Institutions may resist returning money or giving a discount far beyond what makes immediate financial sense in the face of losses from a public campaign— for the same reason corporations will fight unionization even at the cost of missed profits, precisely because they recognize and fear the power of organized people far beyond the demands some people may presently be making.  But for the most part there is not the same threat to a company for a smaller portion of their customers or workers to be coordinating around a specific ask.

In any case, we are building solidarity—communication and caring—which can carry over from one sphere (say, almost everyone dealing with the same monopolistic cable/internet provider) that can carry over to another (such as helping a family resist eviction).

The service PWGD will provide is, at the start, saving time.  After the second or third similar situation of helping someone, we may be able to escalate more quickly to a person in the institution with sufficient decision-making power to make things right.  We do this while possibly being more reasonable, calm, and courteous towards than someone advocating for themselves might be.

With limited paid time to do this, PWGD will have to be selective in both what battles we take on and for whom.  A lightweight referral system to prioritize people who are spending some of their time fighting for others in various ways may be needed.  Opening up, when possible, to trusted volunteers could expand capacity considerably.  In addition, providing a way to catalogue issues from overcharges to bullying by police would at least help with the public record, whether or not we have the resources to follow up immediately.

Many of those affected can afford to pay, or gift PWGD to keep any money recovered, and so subsidize recuperation services for those more in need.  Most corporations use the resources they take from us to do us more harm, by corrupting society (as through supporting harmful politicians or spreading propaganda) and concentrating wealth.  Strengthening a force that can hold corporations to account is a good revenge for all of us.

------

To kick this off, a pile of needs where all money recovered can be used by PWGD to help more people:

* Royalties money never distributed by Apress (should be at least $3,000 by now), owed to PWGD itself.
* Fees for 'inactivity' charged by Middlesex Savings Bank (~\$20 of $5/month), also owed to PWGD itself.
* 'Abandoned' asset turned over by Nava credit union to the Commonwealth of Massachussets (~$7,000), taken from Benjamin Melançon
* Tax stimulus payments (~$2,000) owed to Benjamin Melançon, that's a matter of filing taxes which may be too involved for this service, although would be nice to compete with HR Block etc, that lobby/bribe/corrupt to make paying taxes *more* complicated, while we fight to make it less complicated.
* Restore reasonable pricing for RCN Internet (currently \$20-$30/month more than what same service had been for).
* Correct social security earnings record circa 2008.
* Finally claim $1,000 savings bond or somesuch with the Israeli government that matured 20+ years ago.

Do you have an interest in, liking of, or knack for extracting what's due to you from bureaucracies?

Clearly, this job calls for someone who can keep a focus on what is achievable without letting the overly-specific or wild possibilities distract from the work (unless and until these wild possibilities become achievable!)

(One potential distraction that seems congruent with much of this work:  Helping independent contractors with quarterly tax payments, recording demands of employers perchance to challenge illegal classification as contractors, etc.  This could evolve into "the people's corporation" — every small business or gig job you want to take on, do it through The People, Inc and have payroll and everything handled within a giant worker-owned cooperative for every small business, independent contractor, side hustler to get all the benefits of employmnt, of purchasng power, of selling leverage and (as for immediate benefit) being legally in business with vastly less paperwork.) 

******

# Arbitrary decisions by online platforms

A key but oft-overlooked part of the unethical business practices of monopolistic online platforms is having insufficient staffing to address errors they make.

Arbitrary decisions can be particularly harmful to people using online marketplaces (Amazon, Facebook) and payment processors (Paypal).

Some academic discussion of the power and abuse of this power by online platforms can be found in the PDF available here: [Commoning in the Digital Era: Platform Cooperativism as a Counter to Cognitive Capitalism](https://www.researchgate.net/publication/328212416_Commoning_in_the_Digital_Era_Platform_Cooperativism_as_a_Counter_to_Cognitive_Capitalism) (October 2018), DOI:[10.14746/prt.2018.1.7](http://dx.doi.org/10.14746/prt.2018.1.7) by [Jan Zygmuntowski](https://www.researchgate.net/profile/Jan-Zygmuntowski)



# Civil asset forfeiture

Unfortunately, civil asset forfeiture is *so* unjust that we would need a lawyer in nearly every case, it appears.  This may (increasingly) vary by state, and if no other organization is at least giving people a place to report their 

* [Civil Asset Forfeiture: Explained - The Appeal](https://theappeal.org/understanding-civil-asset-forfeiture-e803c59e633b/)

* [EndForfeitureAbuseTX.org](http://endforfeitureabusetx.org/)

* ["Taken" article in the New Yorker](https://www.newyorker.com/magazine/2013/08/12/taken)

------

[The Time Tax: How Government Learned to Waste Your Time](https://www.theatlantic.com/politics/archive/2021/07/how-government-learned-waste-your-time-tax/619568/) published July 27, 2021.  "A six-month wait for unemployment insurance. A paperwork nightmare over an insurance reimbursement. Drug tests for cash benefits. Why is there so much red tape in American life?"

PWGD or a separate legal entity created for this purpose) will use limited power of attorney agreements to combat all the money lost by we the people, on behalf of we the people, because we the people do not have time for all this nonsense on our own. (Or we may have the time but not the skill, or have the skill but have the time in different parts of the day or week or year than is needed— so in the spirit of mutual aid rather than professionalization often we can help each other out. In the spirit of economic justice, however, people taking on this work should not have to do it on a volunteer basis and will be paid!)


Take warrantee repair. If we know seventeen people have had the same problem, we can probably cut through a lot of the red tape at even the best of companies and absolute gaslighting at the worst. And if the problem is bad enough, affecting enough people, we can probably pressure most companies into making it right.
