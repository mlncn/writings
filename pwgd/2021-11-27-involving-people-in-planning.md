# Involving People in Planning

"the intangible dreams of people have a tangible effect on the world."
  — James Baldwin

Finnish architect and urban cultural activist Jyrki Vanamo noted in 2013 when one is not given the option to propose one's own solutions in the place, average citizens, busy with their work, family, and social life, will only bother with the motions of formal participation when planning and implementation threaten their own comfort, their own everyday life. Otherwise, they will only catch themselves dreaming from time to time:

*I wish there were a cycling route along the seashore… What if this plot of land next to the supermarket was a community park? A pedestrian street would work well here, with a lot of nice cafés!*

(Quoted or paraphrased in "[Combining Participations. Expanding the Locus of Participatory E‐Planning by Combining Participatory Approaches in the Design of Digital Technology and in Urban Planning](https://www.academia.edu/25767277/Combining_Participations_Expanding_the_Locus_of_Participatory_E_Planning_by_Combining_Participatory_Approaches_in_the_Design_of_Digital_Technology_and_in_Urban_Planning)" by Joanna Saad-Sulonen, slightly edited in the intro by me here.)

Electing one person to make all the decisions is an objectively bad approach.  None of us is as smart as all of us.  Each of us knows ourselves best, or at the very least no one can trust someone claiming to know someone else better than themselves, so we have to make decisions for ourselves.

We need to make technologies for planning and for exercising power mundane in the sense used by Saad-Sulonen here, following Dourish (2010) and Michael (2000)— commonplace, part of people's daily lives.  Saad-Sulonen makes the distinction between tools for participatory e-planning, imposed from the top down, and the blogs, wikis, and social networks that "have enabled citizens to document, analyse, and organize themselves".  "These mundane technologies are also important tools for emergent self-organized local communities." 



David Wengrow: The freedom to imagine and reinvent the way we live together.
