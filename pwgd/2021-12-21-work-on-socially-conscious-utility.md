un(certain)-compensating work on socially conscious utility

Hello Anthony,

Figured we were conversing in Twitter notifications, might as well take it to the next level.  Have to admit i did not find your phone or e-mail in the source of the, uh, PDF, nor the website but maybe your next.js app was able to determine i don't have money.

So:  I am pitching the other members of my worker coop to focus any spare cycles we have next year on building a crucial utility for social movements to build power together.  Making the pitch to you as i try to clarify my language; i would greatly appreciate your ideas!

Power is organized people, organization needs communication, and control over communication is key to power within organized groups.  The project is to provide for shared control over communication in a fairly straightforward way:  Taking turns over who makes the decision about which messages go out.

This will scale smoothly from rotating a role in a small group to having sufficient participants to apply statistical random sampling in a very large group.

I see this as fundamental to building shared power.  I cannot say with much honesty that it would make a big difference in some of the struggles i am directly involved in, such as supporting unhoused people in the right to exist, where the main problem feels like too small a group of people who give a damn with willingness and capacity to take action— except if it succeeds in a big way, and multiple struggles share the same infrastructure so their public communications can reach everyone— like, the pooled resources of every group to reach everybody door to door monthly, with people able to opt into daily communiques.  There's no guarantee the median person of a large network, even one rooted in justice-oriented groups, would elevate the defense of encampments to a message everyone must hear, but our odds are a lot better than Twitter's algorithms and the establishment media.

The pitch of equal power in communications itself is probably not enough to draw more people in, especially when it is not intuitive to explain random sampling— as Zeynep Tufekci told me a few years ago, "good luck convincing people about the Central Limit Theorem".

But i think it could make a very big difference replacing or revitalizing the end point of social and political movements, where they often go to die— petitions and political parties, the DSA i hope excepted.  Movement communication should belong to the movement, not to specific organizations within movements, and to the extent that tools to allow democratically-moderated mass communication can become the gathering place when issues gain salience, and to the extent some existing organizations and infrastructure providers adopt its API, something that starts as a fairly straightforward app could make a big difference in the direction of politics that gets to the root of systemic problems— our collective wisdom can grow much faster if we are not reliant on media intent on lobotomizing us, or even if we are not reliant on the time, talent, and integrity of people who end up in communication bottlenecks (whether in key positions in orgs or through their personal followings).

I hope you have managed to fight through my run-on sentences, very interested in your thoughts, and if you're interested in helping build we will be raising money— just very unlikely to reach what you could and should make.

Thanks greatly for your time,

ben
