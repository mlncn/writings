This is a place where you can say you want to start a bakery— all you need is a few million dollars, someone who knows how to bake, and someone who knows how to run a bakery— and it's not a joke, it's the whole point of the site.

No website, no technology, can provide you everything you need to organize something.  This will provide one piece, helping you find the others.

Interested in adding more structure than just a way of scaling communication.

It's great to be able to connect to a million people without a million people having unfettered access to you.

But power comes from organization, not merely communication.  


A couple reality checks:

  * The company-wide mailing list is not the source of a corporation's organizational power.



What do you give a damn about?

[                            ]


You and everyone interested in this will be able to send one message a day—total between all of you—to everyone else.

No, scratch that.

"You will be able to send messages to everyone else who says they give a damn about this."

"Everyone else will be able to send messages to you, too."

Anyone who wants to receive every message can do so; this is very helpful for groups just getting started.  Even at the start some people will want to only receive important messages, even if that takes a couple weeks for the rest of the group to think something happening or a plan or an ask that's important enough.

Originally i was thinking that until it reached 100 people we just limit groups to one message a day, from everyone.  The logic is that a anything resembling a scientific sample with fewer people just isn't feasible to take a sample of— you need almost everybody in the sample anyway.  But that's a bad artificial intervention into the system, one that prevents people from using and learning how the tools work.  And we'll want to see how the tools work when it "doesn't matter" so problems don't suddenly crop up right when it does matter.  We won't mislead anyone; when it's a ten person group and all ten people need to be asked, we won't say 'should this go to the whole group' in that case, but we will ask about the other part— should this be part of the conversation that others should reald later, the featured archive?

And anyway, for a 95% confidence level and a confidence interval of 5, it would need to be asking 384 people before the size of the larger group doesn't matter (per [SurveySystem.com's calculator](https://surveysystem.com/sscalc.htm).

There is no purity in this system.  No perfect scientific solution.  It's judgement calls all the way down.  What's our confidence level?  What's our confidence interval?  Do we require a bare majority of those sampled to approve a message to go to everyone?  60%?  40%?  These decisions could be left up to each group, each goal or action or sub-goal or step, but thinking about that every time would be a show-stopper.  At the very least we need to provide sensible, strong defaults, and probably the answer to wanting different thresholds will be set it up on one's own server.

... the follow up to "What do you give a damn about?" could be:

What are you going to do about it?

[                                ]

People coming along and seeing a goal or action they want to support or be a part of have two options:

  1.  Support and be notified of major updates and action requests.
  2.  Participate and be part of ongoing discussion

***

Mechanism for offline participation: Volunteers call or, who are in the area and offline partipant prefers, visit in person.  This is randomized who is asked to be the contact for the person to share the work— and so i can't make up fifteen imaginary friends whom i talk to and vote on behalf of.

***

If we charge a dollar for online verification 
