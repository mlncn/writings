What do people want police to do with their time?  What do they do?

|What People Want Police To Do|What Police Actually Do|
|---|---|
|quit their jobs|assault protesters|
|solve murders|murder people|
|recover people's personal property and money|steal people's personal property and money "legally" through civil asset forfeiture (and illegally by just pocketing it)|
|investigate and stop wage theft|break strikes and pickets and otherwise protect bosses from organizing workers|
|anything at all of value to society|keep their jobs but don't do anything and collect lots of money|

special off-duty edition

|get mental health supports|beat their wives|


