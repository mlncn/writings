
Every news organization lives or dies, ultimately, based on its relationship with its readers or viewers.

For years now, a significant portion of the (potential) audience relationship of most Internet-based media outlets has been mediated by a handful of giant corporations through Google search, Facebook and Twitter social media.

A federated approach based on a protocol called ActivityPub has proven durable and viable over the past five years.  Federated means different servers run by different people or organizations can host people's accounts, and people can see, reply to, and boost the posts of people on the other servers.  The most widely known software doing this is Mastodon, but Pleroma, Friendica, Pixelfed (image-focused), PeerTube (video-focused), and Mobilizon (event-focused) all implement the ActivityPub protocol.  You can be viewing and interacting with someone using different software and not know it— similar to how you can call someone on the phone and not know their cellular network nor their phone model.

The goal of building a social media following of people interested in (and ideally actively supporting) Portside might be best met by setting up your own social media.

This is very doable with the 'fediverse' and Mastodon in particular.  In particular, because the number of people on this ActivityPub-based federated social web has already grown by a million in the past two weeks— and that's with Twitter not yet having technical problems that are sure to come with most of its staff laid off.  With the likely implosion of Twitter, giving people a home that makes sense for them is a huge service in helping people get started— the hardest part is choosing a site to start with.

People fleeing Twitter as it breaks down socially and technically would benefit from your help in getting on this federated social network.  So would people who have never joined, or long since left, Twitter or other social media, but are willing to join a network that is less toxic and is not engineered to be addictive and harmful.

Portside benefits by having a relationship with readers that is not mediated by proprietary algorithms nor for-profit monopolies.  It makes your access on this social network more like the e-mail lists— it is harder for another entitity to come in between and take it away.

But the mutual benefits for the news organization and its audience go beyond all of this.

When people discuss among one another what a news organization has published, a little bit of genuine community forms.

Starting a Mastodon server could be the start of your news organization seeing itself as not only publishing, but building a better place for people to create content online.

The safety and stability of hosting a home on this federated social network gives people a place to build community.

A number of smart people are encouraging news organizations to host their own communities as part of this federated social media.  Here is one example: https://thetyee.ca/Analysis/2022/11/10/CBC-Should-Start-Own-Mastodon-Server/

But news organizations have been slow to adopt, even now with the Twitter meltdown.  This opens up tho opportunity for extra attention and acquiring new followers.

Hosting the server could cost between $50 to $250 a month, but this is definitely an opportunity to provide a pure community benefit (it is an ad-free culture) and seek donations or memberships.

The true cost is in moderation time; if volunteers can start to fill that you are in good shape.  A comprehensive writeup on everything to consider is here courtesy the cooperatively-managed Mastodon server that Agaric Technology Collective chose to join: 

https://wiki.social.coop/How-to-make-the-fediverse-your-own.html

You would be about the first for media organizations.

You would be:

 - giving people a social media home right when they need it
 - literally owning the platform much of your community is on

And it all works because of the federation aspect— Portside does not have to provide a Twitter, TikTok, or Facebook replacement *yourselves*, you instead join the leading contender for all that.

By being bold and early, you will also get media attention and perhaps donations and grants.

The real question is if it would divert scarce resources from publishing, or if the community-managing aspects of this could bring new volunteer (and potentially paid) talent to handle this.

Even one person willing to take on the moderator role for twenty minutes a day to start should be enough to remove any person who harasses people on other servers or otherwise posts racist, transphobic, or other hateful remarks.

Above all, Portside would be furthering your purpose, through means other than publishing, to inform and educate and help people to interpret the world and change it.


******


 - stand up our own instance
 - serve as a way of doing discovery on Mastodon of like-minded people
 - only downside, not too difficult to by accident, to 'unsubscribe me from this list' - it can end because you know which member of the federation you came from, you can track it back, enveryone from that list, could [defederate] from the Portside server.


Just the fact that we could give people a home— including people who are not on Facebook and Twitter because they're a hellscape.  It's a terrible environment to be in.  And we can promise a better environment that's not just ad-free and fascist-free, but you control what you connect with and can discuss in a 

Our objective would be to not just follow Portside but get people to connect across the federation.

If we run a regular feature, here are hashtags we think are of interest, if you think

Here are people of interest to follow, you want your name to be included?  OK, we'll vet it and include it.  It can be a hub for people to grow their social networks 

People take initiative by submitting articles to Portside now, but that's a very narrow channel.  It can only grow in bandwidth we have available.  Everyone have accounts, then it can grow in the number of users.


Portside does not have comments because we know what comments usually look like, and the effort it would require.  But it would be great if Mastodon could change that, and be an initiator of interesting comments.


We already have servers, more than one with May First.

If Portside instance and other people wanted to join with a formal ownership role, we could do that.

(probably could even change the domain easily?)

People want to hook onto something else, as part of founding, not to be the tail on the dog.

Treat it as a proof of concept, that other people can look at to see how it could run.

Would be nice to do a joint effort of Left-serving media.

If a Portside instance exists, you could point to it, and say we would be perfectly willing to dissolve and be part of something bigger, fully join a collective effort.

The federated social web means that projects can start independently and work together.  Mastodon's features to help people be able to change servers means projects can even merge later.

******

Hi, folks. This is directed to several Portside people who have raised the Twitter/Mastodon issue. All of the comments that I've seen or heard point in the same direction. So I've drafted a proposal hopefully reflecting that direction that should eventually go to the moderators, but first for your consideration. Please weigh in with any comments or suggestions.

===== Begin draft proposal =====

This is a recommendation to the Mods collective for action wrt two platforms: Twitter and Mastodon.

    We recommend staying on Twitter for now but providing avenues for those who wish to transition off Twitter.
    We recommend establishing a Portside community on Mastodon.


TWITTER RECOMMENDATION

Background. Twitter is an influential, high-profile micro-blogging site launched in 2006 with (recently) over 200M users. Portside has been on Twitter since 2012, where we have almost 2K followers (subscribers). Our Twitter account publishes a daily index of Portside posts, some of which are forwarded to tens and hundreds of thousands. We have not, however, engaged in the active discussions on Twitter that are its core.

Since Elon Musk – the richest man in the world and FoT (friend of Trump) –  became the sole owner of Twitter, he has plunged it into a deep and possibly existential crisis. Its moderation policy, business model, relationship with advertisers, connections with influential Twitter posters and its own workforce are all in shambles.

Many high-profile users are quitting Twitter and others are considering doing so, looking for a new platform on which to speak and discuss publicly. Others are, at least for now, staying on Twitter, understandably reluctant to abandon the dense web of relationships and vibrant cultures they have worked hard to create.

Recommendation. We recommend that we remain on Twitter for now, but that our Twitter account include with each post information about how one can get Portside in other ways than through Twitter, including email, Facebook, RSS and Mastodon (see below).

We should actively monitor developments on Twitter and consider taking other actions, including possibly deleting our Twitter account if circumstances warrant.

MASTODON RECOMMENDATION

What is Mastodon? Mastodon is a sort of open-source alternative to Twitter, with significant differences. It is similar in that it allows a user to establish an account and to publish short (<500 character) statements. A user may follow other users, whose posts will then show up in their timeline. A post may include a hashtag (e.g., #elons_shitshow), which then becomes a basis for discussion.

Mastodon differs from Twitter in important respects:

    Mastodon is open source, meaning that anyone can use and modify the software, without charge. 

    Mastodon creates a federation of communities rather than a single centralized one. Elon Musk owns Twitter and everything on it and rules it absolutely. Mark Zuckerberg rules Facebook and Instagram. Musk or Zuckerberg can cancel any account at whim, and change the rules in any way.

    Mastodon, by contrast, is a federation of communities, with rules that unite them. Anyone can establish a Mastodon community with its own rules – e.g., standards of acceptable behavior. A user may, in general, communicate with and follow anyone else in the Mastodon federation. A user may, in general, move freely from one Mastodon community to another, taking their connections and publications with them. You own what you write.

    A Mastodon user controls what they see. No ads. No algorithm-dictated content. You see the posts of those whom you follow, in chronological order. You see discussions (hashtags) that you choose to follow.

    Here’s a selection of recent articles that discuss Mastodon at greater length:
    Should I join Mastodon? A scientist’s guide to Twitter’s rival
    https://portside.org/2022-11-14/should-i-join-mastodon-scientists-guide-twitters-rival

    A newbie guide to Mastodon
    https://link.medium.com/AZnXB9P1Yub

    My week on Mastodon
    https://www.theguardian.com/media/2022/nov/16/could-this-be-twitter-without-the-toxic-slurry-my-week-on-mastodon

    CNN: A beginner's guide to Mastodon, the Twitter alternative that's on 🔥.

https://www.cnn.com/2022/11/08/tech/mastodon-twitter-explainer-trnd

All of this has made Mastodon an attractive alternative to Twitter for many people. In the last month, Mastodon has more than doubled in size, to more than a million users. 

Portside has had a Mastodon account since March that parallels what is published on Twitter (it now has 25 followers).

Recommendation. We recommend that Portside host a Mastodon community. (Technically, this would mean that we create a Mastodon ‘instance’ on a Portside server.) Someone would then be able to join the mastodon.portside community (create an account there) to do Twitter-like things. If Elmo created an account it would appear as @elmo@mastodon.portside. Other Mastodon users could follow @elmo, whether they were on mastodon.portside or some other mastodon instance.

Discussion. 

Feasibility

    Cost. The cost of hosting a community is minimal. We already own servers that could run the software at May First.

    Rules. It would be necessary to establish the community rules (most Mastodon communities have good-behavior rules that we could adopt). We might also incorporate some form of our self-definition and mission as elaborated in our tag line and FAQ.

    Moderation. The work of moderating a Mastodon community would consist primarily of vetting members, preventing or removing trolls, bots and other bad actors. Otherwise a community member speaks their own mind. The moderation work might (as an estimate) require several hours/week.


Benefits

    We would further our mission by hosting a left community of individuals who could speak their own minds, connect with each other and engage in conversations and campaigns. 

    We could use Portside’s content and outreach to enhance the life of the mastodon.portside community (and others in the broader Mastodon fediverse). E.g., we could provide listings or recommendations of people and discussions to follow. We could spread the word about people who are seeking followers or attention for their campaigns. 


This proposal is to establish a policy. We will need to make a separate plan for implementation of the policy.

Where might this lead? Might it serve as ‘proof of concept’ for others with similar aims? Might it be merged into a joint effort with others? Might it create new forms of collaboration with writers and publishers? 

All of these are worth exploring. Wherever the exploration may lead, we recommend that we take the first steps.

******

Well said!

A couple friendly amendments.

If you only read one introduction to Mastodon i would recommend this one:

https://www.wired.com/story/how-to-get-started-use-mastodon/

And figure on ~$100/month costs for hosting Portside's own Mastodon.  It will probably be lower and almost certainly will inspire more donations than it costs.  But because of the way federation works, you have to have storage space ready for images and other content posted on other servers on the federated social network that are shared to Portside's.


******


https://activitypub.rocks/
