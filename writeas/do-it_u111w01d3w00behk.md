## Do it.  Dismantle the Minneapolis Police Department and lead us into a non-violent future

Dear Mayor Frey and City Council Members,

My name is benjamin melançon. I am a resident of and homeowner in North Minneapolis, in Ward 5 and the soon to be former 4th police precinct.

I can't get the video of the meeting to work unfortunately and during COVID-19 we shouldn't be there in person, but i wanted to send my strong support for dismantling the police department and local jails, and putting the money saved into support for all residents, especially youth 16-24. 

We will continue to come together to protect each other, just as we have this past week— when we had to protect ourselves from the MPD and opportunistic outsider racists.

Further, i urge you to look into taking strong measures to ensure that every person can live in Minneapolis in prosperity, health, and mental well-being, including:

  * a resident dividend funded by increased property taxes, which would leave most property owners paying a net lower tax rate, houseless people receiving money that can be put toward housing, all while make it economically unfeasible for anyone, especially non-resident/corporate owners, to leave properties empty and unused.
  * a fee for all potential waste coming into the city, literally everything from natural gas to be burnt to soda cans, just like some states have bottle bills, where people get a refund for turning things in, and for anything that is littered or wasted or used up the money is distributed to everyone equally.
  * a community currency to supplement taxation and help the critical need of paying people for helping their fellow city residents.

Please above all adopt a budget that defunds the police and funds community-led health and safety strategies that do not use violence.

Have courage, and thank you greatly,

benjamin melançon