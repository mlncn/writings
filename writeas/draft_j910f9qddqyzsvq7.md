DRAFT:

We expected going online would lead to mass democratization, that the access to information and ability to share information would empower groups of people.

gave people who have wealth and power and own means of production the ability to outsource all the expenses that generate

costs of maintaining fleet of vehicles is off the balance sheet


instead we got outsourced

Anthropology-trained educator Kasey Qynn Dolin just made this point.


Outsourcing and pushing costs onto people as individuals has been the result of our networked world.

Increased democracy has not.

Why?

Information technology makes it easy to compartmentalize (decouple, encapsulate) work.

Information technology does not make it as easy to share power.

Of course, information technology has been developed in the context of our existing power structures and 

But it seems that it is inherently harder 