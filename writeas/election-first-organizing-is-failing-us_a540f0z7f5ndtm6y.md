# Election-first organizing is failing us

## We need to move to movement-first organizing

(Cross-posted to https://agaric.coop/blog/election-first-organizing-failing-us )

Monday night before the "Super Tuesday" primary, I'm searching for "does the bernie sanders app help you offer rides to polls to people" and finding no answer.  (It does not.)

All I found was Lyft offering ride codes to for a handful of non-partisan non-profits to distribute.  If Lyft can realize that simple physical access to vote is a barrier that affects different groups of people unequally and cite [the facts about youth not voting](https://circle.tufts.edu/latest-research/why-youth-dont-vote-differences-race-and-education), it surely came up on the Bernie Sanders Slack.

Yet in this highly online-connected campaign, some of the basic steps to winning (asking everyone:  Do you have a plan to vote?  Do you need help getting to the polls?) didn't make it into the [official app](https://app.berniesanders.com/ ), nor in any public side efforts.

There are a huge number of thoughtful, dedicated people working on the Bernie Sanders campaign (and in other political campaigns), but as in every movement I've witnessed I'm convinced that not all the best ideas are bubbling up.

Even when a goal is simple (get this one person elected president) the tactics are likely to need to be varied and complex.

This is vastly more true when we're talking about a movement.  Even in a presidential campaign like for Sanders, the the goals behind the goal—health care, living wages, lots more jobs for everyone because we're putting people to work reversing global warming—are many, multifaceted, and cannot possibly be achieved only through electing someone, even to an office like the United State's imperial presidency.

After getting over my personal hangup of asking people for something without having at least the barest offer of help (a ride to go vote), I did start texting a few people to encourage them to vote.  But as I texted my brother in New York, I'm still bummed we're organizing in the context of political campaigns, instead of having huge movements that, as an afterthought, choose our politicians.

I'm not making (or necessarily opposing) the argument that electoral organizing distracts from more important grassroots organizing.

I have gotten involved with a [local solidarity network](https://libcom.org/library/you-say-you-want-build-solidarity-network ) which focuses on direct action to help people with immediate problems— frequently a dozen people helping just one person or a few people at a time win livable spaces from landlords (or get security deposits back), or get stolen wages from an employer.

This sort of deep organizing—really only medium deep, but it's using available resources to nearly their maximum capacity—does not have the breadth of the typical mayoral campaign.

We need breadth as well as depth.  There are many problems that can't be solved on a case by case basis.  Although the type of organizing local solidarity networks engage in builds the capacity to take on bigger problems, it doesn't necessarily scale fast enough, or have clear mechanisms to translate built power and solidarity in one area to others.

The question of translating power built in one sphere to another is even more pressing for the election campaigns.

It's no secret, as Frank Chapman of the [National Alliance Against Racist and Political Repression](https://naarpr.org/ ) reminded people in Minneapolis when he visited from Chicago, that you build political power by going door to door and finding supporters.

What would our political movements be able to do if we didn't have to redo all the grunt work every time?

Or if people weren't canvassed only by campaigns (electoral or otherwise), but asked about their needs?

There are enough people who give a damn.

We could build immensely powerful movements from the ground up, if we had a way to agree how shared resources of movements—including communication channels—would be controlled.

To be a movement for, among other things, democracy, we need to be democratic ourselves.  The [DSA](https://www.dsausa.org/get-involved/ ) is probably farthest along in reach and democratic mechanisms, and so a natural place to join.

We need better technology to coordinate to achieve justice, liberty, and better lives for all.  I don't mean merely a better canvassing app.

We need approaches and tools that let us share power.  Then we can truly build power together.

A positive spin on this extremely spun election: media coverage has meant a ton *but advertising has not*.  And the national, corporate media (which, if for instance you haven't checked who owns your local newspaper, if you even have one, is nearly all of the news media) is the sworn enemy to economic fairness and equal political power.  No one with resources should put a cent into our enemies pockets by buying ads, especially when it doesn't even work.

It's a perfect opportunity to build institutions that work for us, rather than pouring resources and energy into institutions that are getting us killed.

We can build a communication network through which we collectively decide what we want, and then figure out how to coordinate to get it— whether it's electing someone or holding politicians or businesses accountable with direct action or forming ourselves into a giant cooperative corporation to negotiate as workers and buyers more equally with the huge corporations we deal with on a day-to-day basis.

If you're in the position to connect us to campaigns, cooperatives, parties, or other organizations who see a need for communication tools controlled by all the people in an organization or movement, where the ideas and control of resources can build from below, please [contact Agaric](https://agaric.coop/contact ).