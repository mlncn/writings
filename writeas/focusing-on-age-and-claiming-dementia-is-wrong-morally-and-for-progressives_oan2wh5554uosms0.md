Focusing on age and claiming dementia is wrong, morally and (for progressives) as political strategy.  It's basic human decency to not attack people for something they did not choose or cannot change or that isn't harming other people.  (Both the inherent and no-harm conceptions of recognizing humanity are valid, but the latter is more powerful.  For example, the more conservative approach to fighting for queer rights focuses on "born this way"; the more liberating approach focuses on fundamental human rights to be who we want to be.)

Still, it's worrying that many people seem to have suddenly decided to share that they have diagnosed what appears to be Biden's frequent inability to express coherent thoughts as an expression of his stutter.

I agree completely with [Sabrina's tweets](https://twitter.com/SabrinaTessEp/status/1235204943855968257 ):

> How to criticize Biden without being ableist:
> 1. don’t mention his stutter or say “he can barely get a sentence out”
> 2. don’t criticize memory loss or similar issues.
> 3. there are so many problems with Biden so please criticize the policy content instead of engaging in ableism.

> Good things to call Biden out on:
> 1. Being a sexual harasser 
> 2. Supporting segregation 
> 3. Voting for disastrous wars 
> 4. Homophobia & transphobia 
> Don’t discredit your very valid critiques by reverting to ableism!

Especially given that some people made the same argument about Trump, that he was in cognitive decline— and whatever the state of his mental faculties it has not at all gotten in the way of his promulgating racist, classist, ableist policies or of nominating reactionary, white supremacist, protect-the-rich-at-all-costs young men to lifetime appointments.

But in the completely (*and temporarily*) media-constructed narrative of Biden's "electability" (which spell check rightly doesn't consider a word) that has made Biden the leading candidate by avoiding almost any discussion of policy, [Tim Faust's point](https://twitter.com/crulge/status/1235665392326062085 ) is valid too:

> if Biden attempts to wave away his mental coherence issues as a 'stutter' than i, a stutterer, am going to use my free pass to fuck up all the time for the rest of my life

The strategic progressive thing to do now is to relentlessly focus on policy— and personnel.  We'd need some help from Sanders on that latter part.  Biden is in theory much better positioned to build a "dream team" to take on the Trump regime, so an inability or unwillingness to do so now should be disqualifying.  But Sanders too should be able to have a person to name for agriculture, housing, energy, commerce, labor and every cabinet position who can powerfully advocate for (respectively) non-factory farms and farm workers, non-polluting energy policies, small businesses, workers, and everything else that makes up a progressive change on the federal level.

Having so many official representatives is a great way to get some of the vital so-called earned media, too.

We need to relentlessly press in social media and directly on Biden and his representatives and the media that's supposed to be representing us to get to the policies that a president will enact, and not let Biden's incoherence prevent getting to what his policies are or distract from the lack of humane goals and policies.

Even if every Alzheimer's doctor in the country were to declare some iron-clad diagnosis, focusing on such claims harms disabled people and the elderly and does nothing to advance our long-term goals as people who give a damn about making a better world for all.

In any case, in the primary (unlike the general), Biden's well-paid defenders (including many in the media who managed to spend three different news cycles on Sander's evidently minor heart attack) will be able to deflect any questioning of Biden's capacity to govern as an attack on his disability.

If only it were so easy for us.

"Bernie Sanders' lifetime of advocating for policies that benefit the majority of people in the United States, even when they haven't been politically viable, is a condition known as *integrity*, and it should not be held against him in an election!"