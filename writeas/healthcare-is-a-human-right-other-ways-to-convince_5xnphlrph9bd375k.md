## Healthcare is a Human Right, but other ways to convince?

### Amy Vaz:

How can we do a better job selling #MedicareForAll ?  I have been thinking about this quite a bit over the past two years. Our main line, “Healthcare as a human right,” Doesn’t speak to people on the other side. It just doesn’t, But there are many other untapped elements, that do.

The bottom line is that there are not enough of us lefties to really move the needle on this, BY OURSELVES. Healthcare is something, around which we should be able to coalition build, across the ENTIRE spectrum... A little bit of introspection: I don’t think we’ve done this well.

This is one of the topics @AnandWrites and @AndrewYang discussed in this great interview....  I agree with their approach of making the business case for it, although that is often frowned upon on the left, I think we need to do it, In order to expand the support base.

I also think there is something that is going to be a necessary step in this project, which I don’t believe we have done well or even at all, and that is to sell it to doctors, or at a very minimum actively work to dispel the myths / industry talkingpoints, geared towards them.

### mlncn:

It might need to be rephrased, but healthcare IS a human right and reaching people who truly disagree will be as inefficient as our current health insurance disaster.  The large majority even in the US believe this and we need to make achieving universal healthcare seem possible.

That said, @stephen_verdant's Cognitive Politics has some excellent thinking on reaching across divides from very different ways of thinking, and definitely the coalition for health care for all will need to reach more people.  The book is worth getting: https://www.cognitivepolitics.org/

Interested in who you hope to reach in more detail, @amyvaz3, and i'll re-read parts of Cognitive Politics with that in mind.

But like occupying a hospital only serving wealthy people, if any are left in COVID-19 times (the system's not entirely broken), will move the needle most!

### Amy Vaz:

That looks interesting. The book looks good. I agree that Covid should definitely move the needle, but that aside, I still think we didn’t really get our story straight, during the primary season, and that is when it really mattered. That being said, the fight is NOT over...

Healthcare is a human right, but that is not a talking point that speaks to everyone. We can talk to people about different aspects of it, and it can still be a human right. That doesn’t make it any less so.

I’m not saying to get rid of the phrase, “healthcare is a human right,” I’m saying to ADD on to it, for different audiences.

There are other untapped aspects of having universal healthcare, which could be successful, in different ways, speaking to different people...

I feel like there was kind of a vacuum created during the primary, and what filled it? Unfortunately, this: “How are you going to pay for it?” And we never really recovered from that. We didn’t have enough of a compelling story to counter it.

### mlncn:

oh yeah that was painful.  The key answer was not made plainly by any primary candidate: the money we're spending badly on the sector can easily cover everything.  Your personal costs will go down.  We need more, taxes on billionaires go up.  But agree more is needed beyond that.

Message i most want out there:  The world has been made disastrous by people profiting from it.  Unfortunately no one has the trillions of dollars worth of damage done to the environment through global warming and more, but there's still a ton of ill-gotten gains out there.

Before another person dies from lack of medical coverage, before another child's growth is stunted from housing insecurity, before billions of people risk starvation or drowning from global warming— we start to take that stolen wealth back.  We build systems that meet our needs.

that's not a direct 'how do we get Medicare 4 all passed in 2021' but being consistent on that larger framing may do two things.  1) scare rich people into granting concessions  2) prepare us all to build power collectively and not scapegoat poor people or immigrants or jews.

Note i said occupying a hospital before.  Threats to privilege are the only things that will get the powerful to voluntarily grant us rights, and building power through mutual aid is both a way to do that and to make the powerful not needed— to not have power.

This analysis is not @stephen_verdant endorsed  ;-)

Nor is it endorsed by my co-op [agaric](https://agaric.coop) nor necessarily by me— before we can take over, run, and hold onto a hospital, we need to be able to protect ourselves from eviction, including from public parks:

https://twitter.com/mplssanctuary/status/1290350170966810624

***

### Clarifications

Amy Vaz:

M4A does not enable us to “take over” anything. Everything would still be privately owned. It’s just a single payer program. It is definitely the compromise/bare minimum. We might in fact, actually need something much more far-reaching, but this should be a doable starting point.

mlncn:

i know; i'm talking about direct action to move the political needle (and to build the skills we need to survive).  It's an entirely separate tactic and any effect of getting scared CEOs to push for m4a as they get convinced its in their business interests would be incidental!