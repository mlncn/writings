# Ideas for our police-free future

## Redistribute wealth ##

Abolition has always been tied to economic justice.  Intertwined with white supremacy, inequality is one of the root causes of the problem of policing.

Even aside from the 'protection of property' basis for policing, aside from the injustice of arresting people who shoplift and not people who underpay wages, there's another question: why do people from one population shoplift (for survival, not for fun, as the latter transcends classes and groups) while people from another population commit wage theft?

We all know this is a matter of wealth inequality.

Even though economic injustice is global and nations hold most geopolitical power, Minneapolis can take significant steps to address the unequal and unfair distribution of wealth that is a key driver of racial inequalities.

A resident dividend (payments to every person living in Minneapolis) funded by increased property taxes would leave most property owners paying a net lower tax rate (with the increased taxes more than offset by the dividend).  Houseless people and renters, meanwhile, would receive money that can be put toward housing.  At the same time it would make it economically unfeasible for anyone, especially non-resident/corporate owners, to leave properties empty and unused.


## Pay reparations ##

Black and indigenous people in Minneapolis (as everywhere in the US) are owed reparations for harassment and physical harm by police, for past and ongoing discrimination, for slavery and genocide.

There are models for doing this locally, chiefly Chicago.  (See [How Chicago became the first city to make reparations to victims of police violence](https://www.yesmagazine.org/issue/science/2017/03/21/how-chicago-became-the-first-city-to-make-reparations-to-victims-of-police-violence/ ) and [Slavery reparations seem impossible but in many places they're already happening](https://www.washingtonpost.com/outlook/2020/01/31/slavery-reparations-seem-impossible-many-places-theyre-already-happening/ ) )

For one option in Minneapolis, city land (and land abandoned by out-of-town owners because of the property taxes funding the resident dividend) could be turned over to a Black and indigenous-led cooperative land trust.

## Universal basic income ##

Money doesn't solve all problems, but that's no excuse for not giving enough money to people to solve what problems it can.

The modest wealth redistribution of a resident dividend can also be considered to serve part of this purpose, as it can and should be paid out at least quarterly.

But Minneapolis should further raise what it can towards a weekly or monthly guaranteed minimum income.

An excellent source of revenue for that is charging people for the harm their pollution causes.

Minneapolis could impose a fee for all potential waste coming into the city, literally everything from natural gas to be burnt to soda cans.  Similar to how some states have bottle bills, people get a refund for turning things in, but for anything that is littered and lost or wasted or used up the paid-in money is distributed to everyone equally.


## Neighborhood connectors ##

Another form of prevention, organizers or coordinators who are paid to simply know their neighborhoods, and connect people to resources formal and informal. 


## Community security ##

Different people trained to respond to intervene in different situations of crisis.

Different crisis call for different responses.  You've already started to identify this in the analysis of calls to 911, but lets not forget how rarely many people call 911 at all.  There's lots of situations, such as the majority of domestic violence, that aren't reflected in current data.

Crisis situations can be handled in part with a central number dispatching the relevant professionals for mental health emergencies, interpersonal disputes, physical altercations, injuries, etc.  These professionals would be drawn from or at least required to live in the immediate neighborhoods they serve, allowing both rapid response and an increased chance of cultural awareness and knowledge of particular people.

This must be supplemented by broad training in the community so we can all help each other with first aid, peer therapy, and the like.

If we have a generic all-purpose first responder, they will not have a gun, and de-escalation and first aid will be the main training.

Much like a having a standing army leads to its use and abuse, at home as well as abroad, we cannot repeat the mistake of having even a small full-time armed force in our city.  For the relatively rare occasions when physical intervention is needed to stop someone in the act of doing harm, we can turn to people trained and paid to be on call.  But people with physical intervention training (weapons-trained or not) will neven be on a full time patrol, as that leads to viewing every person they interact with as a potential threat.

Above all, there can be no special rules that apply to people in the role of protector.  We can never again be in a situation where the person with their knee on someone's throat is "doing their job" and someone rushing in to stop this harm could be killed or arrested for assaulting an officer.

The law must apply equally to all.


## No harm, no crime ##

Make clear in policy (and update local ordinances accordingly) that there will be  no restriction or imposed cost on acts which do not coercively harm others.

Most obviously this brings interactions between consenting adults, including selling drugs and sex work, into the realm of using the same dispute resolution practices that are (or will be made) available to everyone.


## Restorative justice and community accountability ##

Policing has failed people facing intimate partner violence so thoroughly it is where some of the most thought and work has gone into alternatives:

https://www.chicagoreader.com/Bleader/archives/2017/11/22/is-it-time-to-reimagine-justice-and-accountability-for-sexual-misconduct

Our communities' capacity for restorative justice must be sown and grown to replace prosecuting people as criminals.


## Free therapy ##

If we're all going to be more able to help people with their own mental wellness challenges, we're all going to need therapy.

[Mental Health Minnesota's "warmline"](https://mentalhealthmn.org/support/minnesota-warmline/ ) may be a model to adopt/expand.


## Guaranteed jobs ##

For people aged 16 to 30, the City of Minneapolis should guarantee a job for anyone who wants to work.

The city should borrow massively at close to zero percent interest to invest in public works, including along the lines of the Green New Deal, as part of the way of fulfilling this jobs guarantee.


## Support what's already working ##

Women's shelters, substance abuse programs, youth enrichment and employment programs, mentorship, and so much more, should find their work much easier with everything else the city starts doing for its residents.  Nevertheless, Minneapolis should be sure to continue and expand support for anything with evidence that it's working.


## Paying for neighborhood connectors, community security, community accountability facilitators, free therapy, guaranteed jobs, and other social supports ##

How to pay for for all this?  Having the entire police budget will help a lot, but continually re-creating a wonderful city that has all its key needs met should not be done on the cheap.

A community currency to supplement taxation can help the critical need of paying people for helping their fellow city residents.  Indeed, Minneapolis could have its first income tax, steeply progressive but payable solely in our own city-issued currency, to kickstart demand for the alternative currency money we can in part use to pay people for all this local work— there's no need for anyone to fight for mal-distributed US dollars to pay for local needs.


## Continuing the visioning and learning process ##

We'll need to deepen our democracy to succeed.  In the immediate term we need to publicly collect people's visions and collectively work on organizing these resources for our near-term abolitionist future.

[Agaric technology cooperative](https://agaric.coop/) can help with quickly setting up a wiki for that. 

*Note: This was written with input from just a few other people and does not represent any community consensus.  It is just one response of the thousands undoubtedly elicited by the call for visions put out to in-person and remote attendees of [the Path Forward Meeting last Sunday in Powderhorn Park](https://unicornriot.ninja/2020/nine-council-members-vow-to-disband-minneapolis-police/) organized by [Black Visions Collective](https://www.blackvisionsmn.org/) and [Reclaim The Block](https://www.reclaimtheblock.org/home).*