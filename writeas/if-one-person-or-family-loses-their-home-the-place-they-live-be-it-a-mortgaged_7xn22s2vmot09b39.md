If one person or family loses their home—the place they live, be it a mortgaged house or rented apartment or squatted bit of park—because of an unfair and uncaring economic and political system, that is an injustice.

If 9,000,000 (again— see 2008 financial crises) or 90,000,000 people are about to, that is fuel for a rebellion against the unjust system.

Tell your neighbors and people in your community they are not alone.  Find on organization – https://righttothecity.org/about/member-organizations/ – or find or start a solidarity network – https://roarmag.org/essays/us-housing-solidarity-networks/ – to help organize eviction defense.