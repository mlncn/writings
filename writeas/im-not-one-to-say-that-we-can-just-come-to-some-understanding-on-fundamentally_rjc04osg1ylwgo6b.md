i'm not one to say that we can just come to some understanding on fundamentally different conceptions of fairness and freedom but even some of the people literally yelling for (more) inessential businesses to begin serving their wants and/or making them money, and truly a supermajority of all people even in the United States, can be brought around to a couple basic points:

1. Everyone should have the resources needed to live no matter what the global or personal economic, medical, or social conditions

2. We need near-universal, regular testing for COVID-19 to safely stop staying at home as much as possible.

Like how hard is it for us all to be on team "let's stay alive and not go broke"?

So lets put the pressure on all who hold power to send us all money for the foreseeable future and to get the testing to full capacity NOW— or nothing goes back to normal for anybody.

And our ability to put pressure on will likely depend on us dismantling our local police department, but pressure starts with a demand, and we're already allowing that to get muddled.  Healthcare (including COVID-19 testing), housing, nutritious food, safe water, and other essential human rights are not negotiable demands.  Right now it is also clear that the right to stay alive includes money, COVID tests, personal protective equipment for all non-remote workers— and being freed from prisons and jails.


***

cut for length and clarity (ok, ok, stop laughing):

And every moment we allow scapegoating of each other (people goaded to ridiculous action by evil megalomaniacs and a willingly servile media army; immigrants; other countries) to deflect from putting pressure on all our putatively elected and non-elected holders of power ...  [is a moment not working for true liberation or whatever]


***

[in response to someone on Facebook saying money doesn't come from nowhere and represents people's skills and if there's no one working there's nothing to buy]

 and as long as people have money to meet their needs, there will be people who find a way to make it.  And the US government is making dollars appear out of nowhere by the literal trillions right now, but giving it to huge investment banks, corporations that are themselves as much financial scams as producing goods and services, car dealerships owned by congresspeople— and yeah all that money does represent value that people generate using their skills, value that the people being given this money will be able to extract from all of us who are not getting it (a problem long existing but that must be ameliorated not exacerbated by this current crisis).   What needs to stay closed is anything where people gather in person, not every factory, and we need tests and PPE and health care to even keep those open safely.  To dodge that point is to be arguing for tens of thousands of additional deaths, of people who do not have to and should not die.