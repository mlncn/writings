Psst OK it's midnight now i can tweet this.  Some of the circuses are shutting down and in some places it's hard to get bread.

The time for organizing— and for a short period, especially digital organizing, is here.

We need to build power and we need to build ways to share it.


Corona virus is a crisis and it's terrible and do everything you can to keep you and your loved ones and everyone else safe.  Wash hands and stay home as much as possible and all that, but also...

Move public opinion and policy on things that will save lives right now & longterm


Like, stop people from getting evicted by the rich and the courts and police who serve the rich:

https://twitter.com/CityLife_Clvu/status/1237775018672697351


Move toward prison abolition and decarceration by demanding jails in this crisis reduce pressure to what they can deal with by releasing older and other at-risk inmates:

https://twitter.com/NazgolG/status/1237084635433238528


And work to put a freeze on entangling more people in the judicial system in the first place:

https://twitter.com/radleybalko/status/1237755639146917890


And do whatever you can, where you are, like by getting food and necessities to others

https://twitter.com/miamingus/status/1237900119338405888


And yes, in the name of harm reduction, work to get @BernieSanders elected President and other candidates elected and to get near-universal public support for universal public programs and benefits

https://twitter.com/AOC/status/1237900019350421506


And build communication networks that break through the levels of propaganda that keep us in one of the lower levels of hell— communication that builds organizing that builds power.  It's vital that we share control over this communication to share power.

https://agaric.coop/blog/election-first-organizing-failing-us


A huge thing in the #CoronaCrisis and for the future of humanity is to get people out of prisons and jails and take care of people in their communities

https://twitter.com/VOCALNewYork/status/1237869663020707840


Tech i want to help build to build networks of communication that keep the power we build together shared among us, that doesn't exist yet, that i can find.

But the tech to help one another now sure does exist:

https://twitter.com/loud_socialist/status/1237937468722352129


We don't have much power.  But there are many places we can apply pressure— such as pushing more businesses, universities, and towns to provide paid sick leave to their workers— immediately helping people now, and establishing precedent for long-term improvements.


More on such efforts:

https://twitter.com/HongPong/status/1237962809977253888

Oh and https://libcom.org/library/you-say-you-want-build-solidarity-network


I definitely meant to have an #AbolishICE resource in this #coronavirus action list.  @ConMijente is one organization for that.  Here's the reason to give people in their own self-interest, that may also help people see immigrants as human:

https://nitter.net/matt_cam/status/1237939272839282688


Covid-19 hitting is making people viscerally realize we need to rebuild the social net shredded by reactionary policies (implemented by Republicans and the Clinton/Biden side of the Democrats) over decades.  (But the net was never great; we need a social floor no one falls below)

and part of that is restoring resiliency as a value more important than ephemeral efficiency:

https://twitter.com/dellsystem/status/1237933116427132928


Philidelphia looking at keeping people safe (from compounding the effects of poverty) in their homes throughout the epidemic.  What about your city?

https://twitter.com/jblumgart/status/1238120512334725126