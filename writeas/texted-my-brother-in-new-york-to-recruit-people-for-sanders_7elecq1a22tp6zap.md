# Texted my brother in New York to recruit people for Sanders

(This was on Monday night.)

#### Me

do you have the BERN App? https://berniesanders.app.link/

to get everyone in NY registered in time for your late and possibly decisive primary!

#### Brother

Unfortunately it looks like the same thing that happened to Bernie in 2016 will happen again in 2020.

#### Me

Not if half the people we're counting on to join Bernie in voting out Trump and his rubber-stampers in Congress actually start paying attention and showing up in the primaries.  It could really come down to NY.

(mostly agreed but it'll be really hard for them to pull that with a large lead in delegates and growing momentum, so again, New York!)

#### Brother

Hopefully you're right. It looks grim with many prominent Democrats openly not supporting Bernie.

#### Me

that's exactly why this hinges on people who *don't* usually vote—and have lots of good reason to dislike most Democrats because those Democrats never cared about them.  And have reason to question the fact that others, even if admirable public servants, have not delivered the goods of needed change.

(and still bummed we're organizing in the context of political campaigns, instead of having huge movements that as an afterthought chooses our politicians, but we have what we have, and Bernie's campaign is pretty good)

#### Brother

It was good the first time around as well. Hopefully people are more educated now than in 2016.

Maybe an app that has all the candidates voting history🤔 🤔

#### Me

There's tons of websites with that stuff.  Probably some apps.  I do wish yeah like the BERN app had easy access to facts like voting records for all the politicians that might come up in a conversation about going to vote, but as i understand it the best use of time is to just get people moving who normally aren't, not try to change minds with facts.

#### Brother

True indeed

#### Me

But the app i want is to collect what *people* want, not what politicians have done, and once we collectively decide what we want, we figure out how to coordinate to get it— whether it's electing someone or holding politicians or *businesses* accountable with direct action or forming a giant corporation and buying community land / healthcare together and making the corp a monopoly through which we sell our labor since the laws favor monopoly corps over unions!

#### Brother

Sounds like a great plan many people would want to be a part of
