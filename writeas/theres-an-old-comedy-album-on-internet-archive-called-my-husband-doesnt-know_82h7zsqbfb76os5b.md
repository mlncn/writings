There's an old comedy album on Internet Archive called [My Husband Doesn't Know I'm Making This Phone Call: Starring Fannie Flagg as "Little Martha"](https://archive.org/details/lp_my-husband-doesnt-know-im-making-this-phon_fannie-flagg) and i looked up who this John and Martha were— Richard Nixon's attorney general John Mitchell and his wife.

The album is from 1971, and makes fun of Martha Mitchell's outspoken reactionary views— real-life examples including quoting her husband saying protests to end the war against Vietnam looked like the Russian revolution, and reportedly saying Senator Fullbright should be crucified.  Politics was never polite.

She also regularly shared gossip with reporters.  And in 1972, despite attempts by her husband to isolate her, she recognized one of the Watergate burglars as part of her husband's Committee to Re-elect the President and called legendary White House reporter Helen Thomas.  The phone was taken from her, and she was beaten.  She died less than four years later, at age 57.

Her 'handler'—her captor—at the time was former FBI agent Steve King (apparently no relation to the fascist U.S. Representative), and King repeatedly assaulted her.  This violent kidnapper who covered up for Nixon's crimes was appointed to an ambassadorship by Trump, and i didn't even know it until reading [Martha Mitchell's Wikipedia page](https://en.wikipedia.org/wiki/Martha_Mitchell).

Martha's story in detail at [Necessary Storms' The Silencing of Martha Mitchell](https://www.necessarystorms.com/home/the-silencing-of-martha-mitchell).