## Liz Warren or Bernie Sanders on Super Tuesday?

*Discussing voting on Super Tuesday primary with a friend who, it turned out, leaned toward Liz Warren but wanted to hear a case for Bernie Sanders.*

#### Me:

Hi [name of friend i haven't talked to in a year or so because i'm a terrible person], election day is tomorrow! It looks like your polling location is ___.  Will you vote for Bernie?

#### Friend: 

Hi Ben! I'd love for Bernie to be president but I'm terrified of him running against Trump... I thought Warren would do better against Trump but it doesn't seem like she's gonna make it. I'm relatively positive she will endorse Bernie and drop out if she doesnt make a come back Tuesday so I was considering voting for her. I have supported Bernie's campaign so far and will happily vote for him against the Trump, but I feel like voting for Warren will ultimately have the same effect and I will have voted for the candidate I legitimately think stands a better chance. (I have seen projections about Bernie but I dont u understand the logic) 

I would be ecstatic to see Bernie beat Trump. I have a hard time seeing how he's gonna get the Republicans, Liberterians and the center crowd...

If you think voting for bernie as opposed to Warren tomorrow would have a drastic effect on the end result I'd love to hear your perspective.

#### Me:

are you...  are you...  the mythical not-entirely-decided voter?!?  I could have sworn you were a concoction of the horserace-obsessed media!  ;-)

OK so here's the deal.  For reasons that are not at all clear to me either, Bernie Sanders has far more grassroots support than Liz Warren.  She is by far the better debater, some of her policies are better or at least better defined, but Sanders' *consistency* is probably what has attracted the more active supporters and definitely will be a *huge* advantage against Trump.  What he and the billions of attack ads will do with Warren's past as a Republican and identifying as Native American (despite having no tribal or cultural connection, and many Native American communities are still highly not OK with this).

And that's the real thing though— even with truth being completely optional this political era, the ways that Bernie will be attacked will be as a communist and socialist and un-American radical— but the more they paint him like that, the more it is likely to bring *the large majority of people who have tuned out of US politics because it does nothing for them* into the picture.  (While Bernie patiently explains that his policies are indeed what most people across the political spectrum want, it's not at all radical by world standards, and it's deeply part of US political tradition— the Green *New Deal* and all that, and oh yeah the threat of global warming and lack of health care and inequality killing millions of people requires big actions.

Hillary Clinton made a decision to pursue allegedly moderate Republican votes in 2016's general election.  I felt that was the immoral decision at the time— the moral decision is to speak to the disenfranchised, the people who have gotten so little from either political party that they don't vote at all, and move the conversation in a progressive direction (for which there are large majorities for universal healthcare, decreased inequality, better jobs and more control at work— Sanders even talks about worker ownership! but i digress.

What i didn't expect was that Hillary's 2016 decision was *strategically* wrong.  All the Republicans voted for Trump, even though Hillary was everything a Republican could want, economic policy wise, without the incompetence and blatant racism and sexism and xenophobia and general smell of sulfur.

So Bernie *will* bring more people out to vote than Trump can.  He will win, unless large numbers of conservative Democrats vote for Trump or stay home.  But despite the concentration of conservative Democrats in office and in party apparatus, the last election made clear that the parties have finished to near completion a long realignment of white nationalists in the Republican Party and ... non-white nationalists in the Democratic.  Trump is so uniquely evil to any thinking person that this is a historic opportunity— outside the media, which are already full of the 23 or so "never-Trump" Republicans basically spending the last four years pretending to be Democrats, but whose votes we really never had, very few "moderate" Democrats are likely to defect (i pray, i pray, i pray) and vastly more non-voters will be there for Bernie because he offers something.  Again, the sort of attacks that will mostly be made on Bernie have less potential of suppressing voters from the under-represented demographics that we need to turn out than the attacks on Liz Warren would— it would be unfair, but former Republican Warren could be painted as an establishment candidate, in ways that will be much harder to do to Sanders.  Trump is now the establishment, but he's going to try to run as anti-establishment, like last time.  Even with massive media cooperation, it won't work.

(Also i totally have Signal on my computer and i am so so sorry if you are trying to type on a phone, you can call me  :-P  )

#### Friend:

Haha, I'm generally never fully decided until the moment of action :p
Warren being the better debater and having defined plans is what attracted me. I hope that Bernie can calmly distinguish himself from the "socialist" light Republicans see him in.

Very interesting take on Hillary. I saw her loose ground as soon as she stopped pushing to the left. I noticed many who never vote lose interest again. I can see where the projections might be coming from now, it's mostly from those who dont normally vote!

I think Bernie needs to speak more about the green new deal (or some part of his plan) bringing jobs to rural areas, he needs to touch on the job issue that so many lower class Republicans are tied to.

I see your point! Thanks for giving me a hopeful perspective on how Bernie can actually win!

You're right that warren can be turned into the establishment and therefore demotivating people who don't vote from even voting again like Hillary last time...

Haha yes, its terrible typing on the phone, no problem, thanks for taking the time to discuss it!

In terms of having clear plans and better defined policies, I value that but I'm realizing it's not really what politics revolves around sadly

Andrew Yang had me for a while for how logical he was with things, I wish there was a mixture of him with Bernie's ideals

Alrighty, I think I'll just vote for Sanders and reaaaally hope there is a huge turnaround on number of people who vote this year in support of him!

Those who don't ordinarily vote aren't even accounted for in the polling, for the most part!  But yeah polls are bunk.  Always vote your conscience (*and why in the hell do we not have instant runoff voting at least in primaries already*, gahh).  But the thing Bernie keeps repeating about it taking a movement to change society is absolutely true and one has sort of coalesced around him, more than anything we've seen in the US since maybe FDR—of course, then we had a much stronger pull from the left then—but win or lose Bernie's campaign will do more to bring life to the movements we need: labor, radical politics, green movements.  And we're at the delicate point where he needs every vote in the primary, to win it and to burst out into attention of the never-voted-before crowd.

Yes, i just texted to a friend that i cannot believe neither Sanders nor Warren adopted Yang's guaranteed minimum income plan, at the very least!  It was such a clear, focused campaign it was just asking to be incorporated by another candidate.  Mystifying to me.

Thanks so much for chatting i wish i could be moved to reach out to people more without a sort-of-movement moving me to, heh.  I hope you do vote Bernie when the moment of action comes at the voting place!  You have definite plans to get there and all?  :-)

#### Friend:

Yeah! The guaranteed minimum income and restructuring of what GDP to account for support of next generation, health, etc. Would have done wonders for their campaign. Would have brought over so many on the line Republicans and Liberterians

Yes, it's 5min from my house, I'll definitely go vote tomorrow! Definitely either Bernie or Warren, but you've definitely tipped me over to Bernie so most likely him!

No worries, I'm glad you got in touch! Always great hearing from you :)

Haha, yeah! It filled in a few gaps for me in terms of Bernie's potential to beat Trump.

#### Me:

And yes, do vote for Bernie!!  I'm bummed it's 2020 and we're organizing in the context of political campaigns, instead of having huge movements that as an afterthought chooses our politicians (the tech i've always wanted to build is to collect what people want, and once we collectively decide what we want, we figure out how to coordinate to get it).  But we have what we have, and Bernie's campaign has the best potential for symbiosis with true movements.  And this conversation is the absolute highlight of my minuscule voter outreach career!

#### Friend:

Yes I agree, you would think we would have come up with a better system... I've thought about similar techs a lot, would be good to talk more about it in the future! People have to stop making scary evil AI movies! Its brainwashing people to not want tech involved in politics...

That was great! Thanks again for reaching out :)

#### [The next day.]

#### Friend:

Hey Ben, one more thought to run by you. There is an idea that if Warren stays in the race, she will collect more votes from those who wouldn't vote for Bernie but would to her and eventually endorse him with those votes helping Bernie like Biden just got boosted from the others dropping out. This would be purely a strategic decision to help get the more voters for Bernie. Or you think the full boost on Bernie is necessary right now to keep the far left who dont vote motivated?

#### Me:

Ideal:  Warren stays in the race, more people get involved, and Sanders gets a majority before the convention anyway.  She is simply fantastic in the debates (and having more people stay in with actual good ideas would be better, like Yang and like Castro who had a great campaign and is now all in for Warren, and heck even Booker would have been nice to have in the race and up on the stage).

The reality is that *the literal billionaires* and the Democratic establishment working so hard to squash a progressive challenge.  Given the field, why didn't they coalesce around Warren to stop Sanders already?  So they're likely to try to keep this from Warren and Sanders right up through the convention— and after the first round of voting, delegates are *supposed* to change votes to get someone a minority, but no one fully controls their delegates at that point.  Warren can't ensure her delegates switch to Sanders, so our best bet to get someone who isn't a straight shill for the richest 1% is voting Sanders now.

So to be clear, i think we need Bernie in the general to have the best chance in the general election and downticket races and non-electoral organizing, and voting Bernie now, including in Massachusetts, is the best way to have Bernie in the general.

#### Friend:

Makes sense, I guess I shouldn't play into a strategy given it's not a sure thing Warren's delegates will go to Bernie and still hope she stays in to gather the votes of those on the line and endorse him in the future

#### Me:

Thanks again for talking through all this.  Would you be OK if i posted the conversation (lightly edited and anonymous)?  I feel all the posts that aren't two human beings discussing something are...  less helpful  for the common ground we need.  I also have no personal website actually working right now so i'm about to start a free (and libre!) write.as account

#### Friend:

Sure go for it! Looking forward to more conversations in the future :)
